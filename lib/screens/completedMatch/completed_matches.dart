
import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/model/MatchRoundsData.dart';
import 'package:ekojpubg/utils/app_utils.dart';
import 'package:ekojpubg/widgets/rounded_button.dart';
import 'package:ekojpubg/widgets/small_rounded_btn.dart';
import 'package:flutter/material.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';

class CompletedMatchesScreen extends BaseStatefulWidget{
  List<MatchRoundsData> completedMatchRounds;
  CompletedMatchesScreen(this.completedMatchRounds);
  @override
  _CompletedMatchesScreenState createState() =>_CompletedMatchesScreenState();

}


class _CompletedMatchesScreenState extends BaseState<CompletedMatchesScreen> with BaseStatefulScreen{

  @override
  String screenName() =>null;

  @override
  Widget buildBody(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(left:16.0,right: 16.0,bottom: 100.0),
      child: ListView.builder(
          itemCount: this.widget.completedMatchRounds.length,
          itemBuilder:(context, pos){
        return Column(
          children: <Widget>[
            completedMatchesItem(this.widget.completedMatchRounds[pos]),
            Divider(
  thickness: 1.0,
              color: Colors.black,
            )
          ],
        );})
    );
  }



  Widget completedMatchesItem(MatchRoundsData matchRoundsData){
    return Container(
      padding: EdgeInsets.only(top: 8.0,bottom: 8.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceAround,

            children: <Widget>[
             Text("${matchRoundsData.match.name} ${matchRoundsData.rounds.id} ${matchRoundsData.rounds.roundName}",
             style: TextStyle(
               fontSize: 28.0.sp,
               color: Colors.black
             ),),
            matchRoundsData.rounds.matchDatetime!="" ? Text("Starting Time - ${AppUtils.formatTime(matchRoundsData.rounds.matchDatetime)}",
                  style: TextStyle(
                      fontSize: 20.0.sp,
                      color: Colors.black
                  )).paddingAll(top: 12.0.h,bottom: 12.0.h):SizedBox(),
              Text("Coins Earned - 45 Coins",
                  style: TextStyle(
                      fontSize: 20.0.sp,
                      color: Colors.black
                  ))
            ],
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
             Text(
               "Ended",
               style: TextStyle(
                 color: Colors.red,
                 fontSize: 22.sp
               ),
             ).paddingBottom(4.0.h),
              matchRoundsData.rounds.matchDatetime!=""?  Text(
                "Date ${AppUtils.formatDate(matchRoundsData.rounds.matchDatetime)}",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 20.sp
                ),
              ).paddingBottom(12.0.h):SizedBox(),
              SmallRoundedCornerButton(
                  (){},"Upload Screenshot"
              )
            ],
          )

        ],
      ),
    );

  }

}


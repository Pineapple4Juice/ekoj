
import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/model/MatchRoundsData.dart';
import 'package:ekojpubg/utils/app_utils.dart';
import 'package:flutter/material.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';

class JoinedMatchesScreen extends BaseStatefulWidget{
  List<MatchRoundsData> matchRounds;
  JoinedMatchesScreen(this.matchRounds);
  @override
  _JoinedMatchesScreenState createState() =>_JoinedMatchesScreenState();

}


class _JoinedMatchesScreenState extends BaseState<JoinedMatchesScreen> with BaseStatefulScreen{

  @override
  String screenName() =>null;

  @override
  Widget buildBody(BuildContext context) {
    return Container(
       padding: EdgeInsets.only(left:16.0,right: 16.0,bottom: 100.0),
    child: ListView.builder(
    itemCount: this.widget.matchRounds.length,
    itemBuilder:(context, pos){
    return Column(
    children: <Widget>[
    joinedMatchesItem(this.widget.matchRounds[pos]),
    Divider(
    thickness: 1.0,
    color: Colors.black,
    )
    ],
    );})
    );
  }


  Widget joinedMatchesItem(MatchRoundsData matchRoundsData){
    return Container(
    child: Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            RichText(
              text: new TextSpan(
                // Note: Styles for TextSpans must be explicitly defined.
                // Child text spans will inherit styles from parent
                style: new TextStyle(
                    fontSize: 28.0.sp,
                    color: Colors.black,

                ),
                children: <TextSpan>[
                  new TextSpan(text: '${matchRoundsData.match.name} ${matchRoundsData.rounds.id}'),
                  new TextSpan(text: ' ${matchRoundsData.rounds.roundName}',style: TextStyle(color: Colors.lightBlue)),
                ],
              ),
            ),
            Text("Starting Time - ${AppUtils.formatTime(matchRoundsData.rounds.matchDatetime)}",
                style: TextStyle(
                    fontSize: 20.0.sp,
                    color: Colors.black
                )).paddingAll(top: 6.0.h,bottom: 6.0.h),
            matchRoundsData.rounds.roomId!=""?Text("ID - ${matchRoundsData.rounds.roomId}",
                style: TextStyle(
                    fontSize: 20.0.sp,
                    color: Colors.black
                )).paddingAll(bottom: 6.0.h):SizedBox(),
           matchRoundsData.rounds.roomPassword!=""? Text("Password - ${matchRoundsData.rounds.roomPassword}",
                style: TextStyle(
                    fontSize: 20.0.sp,
                    color: Colors.black
                )):SizedBox(),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            /*Text("Slot - 15",
              style: TextStyle(
                  fontSize: 28.0.sp,
                  color: Colors.lightBlue
              ),).paddingBottom(4.0.h),*/
      matchRoundsData.rounds.matchDatetime!=""?Text(
        "Date ${AppUtils.formatDate(matchRoundsData.rounds.matchDatetime)}",
        style: TextStyle(
            color: Colors.black,
            fontSize: 20.sp
        ),
      ).paddingBottom(12.0.h):SizedBox(),
           matchRoundsData.rounds.liveStreamingLink!=""? RichText(
              text: new TextSpan(
                // Note: Styles for TextSpans must be explicitly defined.
                // Child text spans will inherit styles from parent
                style: new TextStyle(
                  fontSize: 24.0.sp,
                  color: Colors.black,

                ),
                children: <TextSpan>[
                  new TextSpan(text: '\u2022 ' ,style: TextStyle(
                      fontSize: 28.0.sp,
                    color: Colors.red
                  )),
                  new TextSpan(text: 'LIVE',style: TextStyle(
                      letterSpacing: 1.2,
                      fontWeight: FontWeight.w700,
                      color: Colors.grey)),
                ],
              ),
            ):SizedBox()

          ],
        )

      ],
    ),
    );

  }


}
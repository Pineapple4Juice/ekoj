import 'package:ekojpubg/model/Player.dart';
import 'package:ekojpubg/model/network/GameDetailResponse.dart';
import 'package:ekojpubg/widgets/TeamItem.dart';
import 'package:ekojpubg/widgets/base_appbar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class BookSlotDetailScreen extends StatefulWidget {
  static const routeName = '/book_slot_detail';

  @override
  _BookSlotDetailScreenState createState() => _BookSlotDetailScreenState();
}

class _BookSlotDetailScreenState extends State<BookSlotDetailScreen> {
  @override
  Widget build(BuildContext context) {
    final Data paidMatches = ModalRoute.of(context).settings.arguments as Data ;
    return Scaffold(
      appBar: BaseAppbar(title: "PUBG MOBILE", appBar: AppBar(), elevation: 0),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.only(left: 16.0, right: 16.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(height: 22),
                    Text("Match #665 Squad",
                        style: TextStyle(
                            fontSize: 22,
                            color: Colors.black87,
                            fontWeight: FontWeight.bold)),
                    SizedBox(height: 8),
                    Text("Starting Time - 12:00 PM",
                        style: TextStyle(fontSize: 17, color: Colors.black87)),
                    SizedBox(height: 8),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("Entry Fee - 50",
                            style:
                                TextStyle(fontSize: 17, color: Colors.black87)),
                        Text("Date 06/06/2020",
                            style:
                                TextStyle(fontSize: 17, color: Colors.black87)),
                      ],
                    ),
                    SizedBox(height: 22),
                  ],
                ),
              ),
            ),
            Expanded(
                child: Container(
              padding: EdgeInsets.only(left: 16, right: 16),
              decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    colorFilter: new ColorFilter.mode(
                        Colors.white.withOpacity(0.2), BlendMode.dstATop),
                    image: AssetImage("assets/images/pubg_cover.jpg")),
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  SizedBox(height: 40),
                  TeamItem("Main Team"),
                  SizedBox(height: 20),
                  TeamItem("Substitutes",
                      player: [PlayerModel(), PlayerModel()]),
                  buildJoinButton(context)
                ],
              ),
            ))
          ],
        ),
      ),
    );
  }

  Widget buildJoinButton(BuildContext context) {
    return Expanded(
      child: Container(
        width: MediaQuery.of(context).size.width,
        child: Align(
          alignment: Alignment.bottomRight,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(bottom: 50, right: 24),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Image.asset(
                          "assets/images/ic_coins.png",
                          height: 32,
                          width: 32,
                        ),
                        SizedBox(width: 4),
                        Text("55",
                            style: TextStyle(
                                fontSize: 22,
                                color: Colors.black87,
                                fontWeight: FontWeight.bold))
                      ],
                    ),
                    Container(
                      height: 34,
                      width: 120,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(24),
                          border:
                              Border.all(color: Colors.deepOrange, width: 1)),
                      child: Center(
                          child: Text(
                        "Join",
                        style: TextStyle(
                            color: Colors.deepOrange,
                            fontWeight: FontWeight.w500,
                            fontSize: 18),
                      )),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

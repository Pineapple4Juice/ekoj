import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/home/AvailableGames.dart';
import 'package:ekojpubg/model/home/Banner.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class HomeService{

  Future<List<BannerClass>> getBanners() async{
   ApiResponse apiResponse= await ApiHitter.getApiResponse(AppConstants.Banner);
   try{
     return apiResponse.response != null ? parseBannerResponse(apiResponse.response.data) :  null ;
   }catch(e){
     print(e..toString());
     return null;
   }
  }

  Future<AvailableGames> getAvailableGames() async{
    ApiResponse apiResponse= await ApiHitter.getApiResponse(AppConstants.AVAILABLE_GAMES);
    try{
      return apiResponse.response != null ? AvailableGames.fromJson(apiResponse.response.data) :  null ;
    }catch(e){
      print(e..toString());
      return null;
    }
  }

}
import 'package:dots_indicator/dots_indicator.dart';
import 'package:ekojpubg/commons/FutureConsumer.dart';
import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/home/AvailableGames.dart';
import 'package:ekojpubg/model/home/Banner.dart';
import 'package:ekojpubg/screens/createTeam/CreateTeamScreen.dart';
import 'package:ekojpubg/screens/customer_support/CustomerSupportScreen.dart';
import 'package:ekojpubg/screens/gameDetail/GameDetailScreen.dart';
import 'package:ekojpubg/screens/hire_result/HireAndResultScreen.dart';
import 'package:ekojpubg/screens/home/service/HomeService.dart';
import 'package:ekojpubg/screens/myMatch/my_matches.dart';
import 'package:ekojpubg/screens/notification/notification.dart';
import 'package:ekojpubg/screens/profile/profile_screen.dart';
import 'package:ekojpubg/screens/sponsMatch/sponsored_matches.dart';
import 'package:ekojpubg/screens/wallet/wallet_view.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/home_screen';

  @override
  State<StatefulWidget> createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  ScrollController scrollController;
  double currentIndicatorPos = 0;

  @override
  void initState() {
    scrollController = ScrollController();
    scrollController.addListener(() {
      currentIndicatorPos = scrollController.offset /
          ((MediaQuery.of(context).size.width * 0.75) + 12);
      setState(() {});
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Container(
          width: 150,
          child: Stack(
            alignment: Alignment.centerLeft,
            children: <Widget>[
              Image.asset('assets/images/ic_app_icon_small.png',
                  height: 80, width: 80),
              Positioned(
                  left: 60,
                  child: Text("EKOJ",
                      style: TextStyle(fontSize: 19, color: Colors.white)))
            ],
          ),
        ),
        actions: <Widget>[
          Icon(Icons.account_balance_wallet),
          SizedBox(width: 14),
          IconButton(
            icon: Icon(Icons.notifications),
            onPressed: () {
              Navigator.of(context).pushNamed(NotificationScreen.routeName);
            },
          ),
          SizedBox(width: 16)
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            SizedBox(height: 40),
            Center(
              child: Container(
                height: 90,
                width: 90,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        image: AssetImage("assets/images/pubg_cover.jpg"),
                        fit: BoxFit.cover)),
              ),
            ),
            Center(
                child: Text("EKOJ",
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.bold))),
            Center(
                child: Text("An eSport Platform",
                    style: TextStyle(fontSize: 16, color: Colors.black))),
            SizedBox(height: 24),
            ListTile(
              title: Text("Home"),
              leading: Icon(Icons.home, color: Colors.black),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text("Profile"),
              leading: Icon(Icons.person, color: Colors.black),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).pushNamed(ProfileScreen.routeName);
              },
            ),
            ListTile(
              title: Text("Sponsored Matches"),
              leading: Icon(Icons.line_weight, color: Colors.black),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context)
                    .pushNamed(SponseredMatchesScreen.routeName);
              },
            ),
            ListTile(
              title: Text("Wallet"),
              leading: Icon(Icons.account_balance_wallet, color: Colors.black),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).pushNamed(WalletViewScreen.routeName);
              },
            ),
            ListTile(
              title: Text("My Matches"),
              leading: Icon(Icons.confirmation_number, color: Colors.black),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).pushNamed(MyMatchesScreen.routeName);
              },
            ),
            ListTile(
                title: Text("Team"),
                leading: Icon(Icons.people, color: Colors.black),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context).pushNamed(CreateTeamScreen.routeName);
                }),
            ListTile(
                title: Text("Hire & Results"),
                leading: Icon(Icons.call_merge, color: Colors.black),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context)
                      .pushNamed(HireAndResultScreen.routeName);
                }),
            ListTile(
                title: Text("Customer Support"),
                leading: Icon(Icons.contacts, color: Colors.black),
                onTap: () {
                  Navigator.pop(context);
                  Navigator.of(context)
                      .pushNamed(CustomerSupportScreen.routeName);
                }),
            ListTile(
              title: Text("Logout"),
              leading: Icon(Icons.exit_to_app, color: Colors.black),
            )
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 24),
            FutureBuilder(
              future: HomeService().getBanners(),
              builder: (context,snapshot){
                return streamConsumer(snapshot, (){
                  var bannnerList=(snapshot.data as List<BannerClass>);
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          /* SizedBox(width: 16),
                      Image.asset(
                        "assets/images/ic_bell.png",
                        height: 28,
                        width: 28,
                      ),
                      SizedBox(width: 8),*/
                          Expanded(
                            child: SizedBox(
                              height: 120,
                              child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  controller: scrollController,
                                  shrinkWrap: true,
                                  itemCount: bannnerList.length,
                                  itemBuilder: (context, pos) {
                                    return Padding(
                                      padding: const EdgeInsets.only(left: 8.0, right: 4),
                                      child: Container(
                                          margin: EdgeInsets.only(left: 8.0,right: 8.0),
                                          height: 80,
                                          width: MediaQuery.of(context).size.width * 0.75,
                                          decoration: BoxDecoration(
                                              color: Colors.grey.withAlpha(150),
                                              borderRadius: BorderRadius.circular(10),
                                            image: DecorationImage(
                                              image: NetworkImage(AppConstants.domain+bannnerList[pos].profilePic),
                                              fit: BoxFit.fill
                                            )
                                          ),


                                      ),
                                    );
                                  }),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 16),
                      DotsIndicator(
                        dotsCount:bannnerList.length,
                        position: currentIndicatorPos,
                        decorator: DotsDecorator(
                          spacing: const EdgeInsets.all(10.0),
                        ),
                      ),
                    ],
                  );
                });

              }
            ),

            SizedBox(height: 6),
            Container(
                height: 0.3,
                width: MediaQuery.of(context).size.width)
                ,
            FutureBuilder<AvailableGames>(
              future: HomeService().getAvailableGames(),
              builder: (context,snapshot){
                return streamConsumer(snapshot, (){
                  var gamesList= snapshot.data.data;
                  return ListView.builder(
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: gamesList.length,
                  shrinkWrap: true,
                  itemBuilder: (context, pos) {
                  return GameCard(gamesList[pos]);
                  });
                });
              },
            )

          ],
        ),
      ).noScrollEffect(),
    );
  }
}

class GameCard extends StatelessWidget {
  Data game;
  GameCard(this.game);
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).pushNamed(GameDetailScreen.routeName);
      },
      child: Container(
          padding: EdgeInsets.fromLTRB(16, 10, 16, 10),
          height: MediaQuery.of(context).size.height * 0.32,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage("assets/images/pubg_cover.jpg"),
                            fit: BoxFit.cover)),
                  ).elevation(30),
                  Expanded(
                      child: Text(game.name.toUpperCase(),
                              overflow: TextOverflow.ellipsis,
                              maxLines: 1,
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400))
                          .paddingLeft(8))
                ],
              ),
              SizedBox(height: 16),
              Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(20),
                    child: Image.asset("assets/images/pubg_cover.jpg",
                            fit: BoxFit.cover, width: double.infinity)
                        .elevation(0),
                  ))
            ],
          )).elevation(0, elevation: 1).paddingBottom(2),
    );
  }
}


import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/screens/mobilescrims/slot_pubg_scrim.dart';
import 'package:ekojpubg/utils/responsive/flutter_screenutil.dart';
import 'package:ekojpubg/widgets/rounded_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';

class SponseredMatchesScreen extends BaseStatefulWidget{
  static const routeName = '/sponsoredMatches';
  @override
  _SponseredMatchesScreenState createState()=>_SponseredMatchesScreenState();

}

class _SponseredMatchesScreenState extends BaseState<SponseredMatchesScreen> with BaseStatefulScreen{

  _SponseredMatchesScreenState() : super(defaultPadding: false);

  @override
  String screenName() => "Sponsored Matches";

  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);
    return Container(
      color: Color(0xFFE3E3E3),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: ListView.builder(
          itemCount: 10,
          itemBuilder: (context,pos){
         return matchesItem();
      }),
    );
  }


  Widget matchesItem(){
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text("PUBG MOBILE",
              style: TextStyle(
                color: Colors.red,
                fontSize: 24.sp
              ),).paddingBottom(2.0.h),
              Text("PRICE POOL 50000",
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 24.sp
                ),),
              Text(
                "Starting Time - 12:00 PM",
                style: TextStyle(
                  fontSize: 18.0.sp,
                  color: Colors.black
                ),
              ),
              Text(
                  "Date 06/06/2020"
              ).paddingTop(8.0.h),
              Text(
                  "Match #6565 Squad"
              ).paddingTop(8.0.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Icon(Icons.people,color: Colors.blue,),
                Text("10000+ players have joined",style: TextStyle(
                  fontSize: 16.sp,
                  color: Colors.black
                ),)
              ],
              ).paddingTop(12.0.h)
            ],
          ).paddingTop(8.0.h),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
            Card(

              elevation: 8.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)
              ),
              child: Container(
               padding: EdgeInsets.all(12.0),
                height: 120.h,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("SPONSORED BY XYZ",
                    style: TextStyle(
                        color: Colors.red,
                        fontSize: 24.sp
                    ),),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Image.asset(
                        "assets/images/youtube.png",
                        width: 50.w,
                        height: 40.h,
                        fit: BoxFit.fill,
                      ),
                      SizedBox(width:14.w ,),
                      Image.asset(
                        "assets/images/youtube.png",
                        width: 50.w,
                        height: 40.h,
                        fit: BoxFit.fill,
                      ),
                 SizedBox(width:14.w ,),
                      Image.asset(
                        "assets/images/facebook_circled.png",
                        width: 45.w,
                        height: 40.h,


                      ),

                    ],
                  )
                ],
              )
              ),

            ),
              SizedBox(height: 45.h,),
              Container(
                  //width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(12.0),
                  child: RoundedCornerButton((){
                    Navigator.pushNamed(context, SlotPubgScrimScreen.routeName);
                  },"Book Slot"))
            ],
          )
        ],
      ),
    ).paddingTop(8.0.h);
  }


}

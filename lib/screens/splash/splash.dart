import 'dart:async';

import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/base/base_stateless_widget.dart';
import 'package:ekojpubg/screens/login/login.dart';

import 'package:ekojpubg/utils/responsive/screenutil.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';

class SplashScreen extends BaseStatefulWidget {
  @override
  _SplashScreenState createState() {
   return _SplashScreenState();
  }

}

class _SplashScreenState extends BaseState<SplashScreen> with BaseStatefulScreen {

  _SplashScreenState() : super(defaultPadding: false);
  @override
  void initState() {
    super.initState();
    startTime();
  }

  startTime() async {
    var _duration = new Duration(seconds: 2);
    return new Timer(_duration, navigationPage);
  }

  void navigationPage() {
    print('Timer involked');
    Navigator.of(context).pushReplacementNamed(LoginView.routeName);
  }

  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            setSplashImage(),
            SizedBox(
              height: 20.h,
            ),
            splashText(),
            Text(
              "An eSport Platform",
              style: TextStyle(
                color: Colors.black,
                fontSize: 22.sp,
                //fontWeight: FontWeight.bold
              ),
            ).paddingTop(1.0.h),
          ],
        ),
      ),
    );
  }



  @override
  String screenName() =>null;



  Widget setSplashImage() {
    return Image.asset(
      "assets/images/app_icon.png",
      width: 114.w,
      height: 114.h,
      color: Colors.deepOrangeAccent,
      fit: BoxFit.fill,
    );
  }

  Widget splashText() {
    return Text(
      "EKOJ",
      style: TextStyle(
        color: Colors.black,
        fontSize: 42.0.sp
      ),
    );
  }
}

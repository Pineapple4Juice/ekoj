import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/drawer/MyMatches.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class MyMatchService{
  Future<MyMatches> getMyMatches(userId,game_id,my_match_type) async{
    ApiResponse apiResponse= await ApiHitter.getApiResponse(AppConstants.MYMATCHES,
        queryParameters: {
          "user_id":userId,
          "game_id":game_id,
          "my_match_type":my_match_type
        });
    try{
      return apiResponse.response != null ?
      MyMatches.fromJson(apiResponse.response.data)
          : (CommonResponse()..Message = apiResponse.msg) ;
    }catch(e){
      print(e..toString());
      return null;
    }
  }
}
import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/model/MatchRoundsData.dart';
import 'package:ekojpubg/model/drawer/MyMatches.dart';
import 'package:ekojpubg/screens/completedMatch/completed_matches.dart';
import 'package:ekojpubg/screens/joinedMatch/joined_matches.dart';
import 'package:ekojpubg/screens/myMatch/service/MyMatchService.dart';
import 'package:ekojpubg/utils/responsive/flutter_screenutil.dart';
import 'package:ekojpubg/widgets/ProgressDialogType.dart';
import 'package:ekojpubg/widgets/custom_toggle_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/utils/extensions/context.dart';

class MyMatchesScreen extends BaseStatefulWidget {
  static const routeName = '/myMatches';
  @override
  _MyMatchesScreenState createState() => _MyMatchesScreenState();
}

class _MyMatchesScreenState extends BaseState<MyMatchesScreen>
    with BaseStatefulScreen {
  var _selectedIndex = 0;
  _MyMatchesScreenState() : super(defaultPadding: false);

  MyMatchService myMatchService;
  List<Data> matchList;
  List<Data> completedMatchList;
  List<MatchRoundsData> matchRounds=[];
  List<MatchRoundsData> completedMatchRounds=[];
  ProgressDialog progressDialog;

  @override
  void initState() {
    super.initState();
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    myMatchService = MyMatchService();
    getJoinedMatches();
    getCompletedMatches();
  }

  void getJoinedMatches()async{
    final userId = await SharedPreferenceUtil.getUserid();
    await progressDialog.show();
    myMatchService.getMyMatches("8","1","joined").then((value) {
      if (value != null &&
          value.status == "success") {
        setState(() {
          matchList=value.data;
          matchList.forEach((element) {
            var match = element.match;
            element.rounds.forEach((element) {
              matchRounds.add(MatchRoundsData(rounds: element,match: match));
            });
          });
        });

      }else if(value != null &&
          value.status != "success"){
        context.showAlert("Something went Wrong.");
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  void getCompletedMatches()async{
    final userId = await SharedPreferenceUtil.getUserid();
    await progressDialog.show();
    myMatchService.getMyMatches("8","1","completed").then((value) {
      if (value != null &&
          value.status == "success") {
        setState(() {
          completedMatchList=value.data;
          completedMatchList.forEach((element) {
            var match = element.match;
            element.rounds.forEach((element) {
              completedMatchRounds.add(MatchRoundsData(rounds: element,match: match));
            });
          });
        });

      }else if(value != null &&
          value.status != "success"){
        context.showAlert("Something went Wrong.");
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }



  @override
  String screenName() => "My Matches";

  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);
    return SingleChildScrollView(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  matchesType("assets/images/pubg_mobile.png", "PUBG MOBILE"),
                  matchesType("assets/images/call_of_duty.png", "CALL OF DUTY")
                      .paddingLeft(12.0.w),
                  matchesType("assets/images/free_fire.png", "FREE FIRE")
                      .paddingLeft(12.0.w)
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(right: 8, bottom: 10, top: 14),
              child: Align(
                  alignment: Alignment.topCenter, child: setToggleButton()),
            ),
            Divider(thickness: 0.8,color: Colors.grey,),
            Expanded(
              child: _selectedIndex == 0
                  ? matchRounds!=null && matchRounds.length>0?JoinedMatchesScreen(matchRounds):Center(child: Text("No Joined Matches"),)
                  : completedMatchRounds!=null && completedMatchRounds.length>0?CompletedMatchesScreen(completedMatchRounds):Center(child: Text("No Completed Matches"),),
            )
          ],
        ),
      ),
    );
  }

  Widget setToggleButton() {
    return Container(
        height: 40,
        width: MediaQuery.of(context).size.width * 0.80,
        // margin: EdgeInsets.only(right:8, bottom: 10, top: 8),
        child: CustomToggleButton(['Joined', 'Completed'], (selectedIndex) {
          if (selectedIndex == 0) {
            setState(() {
              _selectedIndex = 0;
            });
          } else {
            setState(() {
              _selectedIndex = 1;
              //this.showListView = false;
            });
          }
        })).elevation(20.0);
  }

  Widget matchesType(imagePath, bottomTitle) {
    return Column(
      children: <Widget>[
        Container(
          width: 80,
          height: 80,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image:
                DecorationImage(image: AssetImage(imagePath), fit: BoxFit.fill),
          ),
        ),
        Container(
            child: Text(
          bottomTitle,
          style: TextStyle(fontSize: 16.0.sp),
        ))
      ],
    );
  }
}

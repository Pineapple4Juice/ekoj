import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:flutter/material.dart';



class TransactionScreen extends BaseStatefulWidget {
  static const routeName = '/transaction';

  @override
  _TransactionScreenState createState() => _TransactionScreenState();
}

class _TransactionScreenState extends BaseState<TransactionScreen>
    with BaseStatefulScreen {
  _TransactionScreenState() : super(defaultPadding: false);

  final list = [
    TransHistoryWidget(
        iconData: Icons.arrow_downward,
        text: "Added",
        date: "15 Aug",
        coins: "100 Coins",
        dollars: "\$1.49",
        color: Colors.green[400]),
    TransHistoryWidget(
      iconData: Icons.arrow_upward,
      text: "Withdraw",
      date: "13 Aug",
      coins: "20 Coins",
      dollars: "-\$2.98",
      color: Colors.pink[400],
    )
  ];

  @override
  Widget buildBody(BuildContext context) {
    return Container(
        color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(itemCount: 2 ,itemBuilder: (context, pos) {
          return list[pos];
        }),
      ),
    );
  }

  @override
  String screenName() {
    return "Transactions";
  }

}


class TransHistoryWidget extends StatelessWidget {
  final IconData iconData;
  final String text;
  final String date;
  final String coins;
  final String dollars;
  final Color color;
  TransHistoryWidget(
      {@required this.iconData,
        @required this.text,
        @required this.date,
        @required this.coins,
        @required this.dollars,
        @required this.color,});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: 16),
      child: Row(
        children: <Widget>[
          RoundedButton(
            iconData: iconData,
            color: Colors.white,
          ),
          SizedBox(width: 20.0,),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                text,
                style: TextStyle(fontSize: 22.0, color: Colors.black87, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 5.0),
              Text(
                date,
                style: TextStyle(color: Colors.grey),
              ),
            ],
          ),
          Expanded(child: SizedBox()),
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Text(
                coins,
                style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                    color: color),
              ),
             /* SizedBox(height: 5.0),
              Text(
                dollars,
                style: TextStyle(color: color),
              ),*/
            ],
          ),
        ],
      ),
    );
  }
}


class RoundedButton extends StatelessWidget {
  final IconData iconData;
  final Color color;
  RoundedButton({@required this.iconData, @required this.color});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70.0,
      height: 80.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(20.0)),
        color: Colors.black87,
      ),
      child: Icon(
        iconData,
        size: 40.0,
        color: color,
      ),
    );
  }
}

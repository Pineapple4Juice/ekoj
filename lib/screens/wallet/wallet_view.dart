import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/screens/wallet/withdraw_view.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/utils/responsive/flutter_screenutil.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';
import 'package:ekojpubg/widgets/small_rounded_btn.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

import 'Transactions_history_screen.dart';

class WalletViewScreen extends BaseStatefulWidget {
  static const routeName = '/wallet';
  @override
  _WalletViewState createState() => _WalletViewState();
}

class _WalletViewState extends BaseState<WalletViewScreen> with BaseStatefulScreen {
  @override
  String screenName() => "Wallet";

  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);
    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: <Widget>[
            profileCard(),
            accountDetailCard(),
            transactionCard(),
            getBottomContainer()
          ],
        ),
      ),
    );
  }

  Widget profileCard() {
    return Card(
      elevation: 8.0,
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              width: 90,
              height: 90,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    image: AssetImage("assets/images/game_icon.png"),
                    fit: BoxFit.fill),
              ),
            ),
            SizedBox(
              width: 32.w,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Chotta Bheem",
                  style:
                      TextStyle(fontSize: 24.0.sp, fontWeight: FontWeight.w800),
                ).paddingBottom(10.0.h),
                Text(
                  "Total Balance",
                  style: TextStyle(color: Colors.orange, fontSize: 18.0.sp),
                ).paddingBottom(4.0.h),
                Text(
                  "Rs 270",
                  style:
                      TextStyle(fontSize: 22.0.sp, fontWeight: FontWeight.w600),
                ).paddingBottom(12.0.h),
                SmallRoundedCornerButton(() {}, "Add Balance")
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget accountDetailCard() {
    return Card(
      elevation: 8.0,
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Column(
          children: <Widget>[
            detailRow(Icons.trending_down, "DEPOSITED").paddingBottom(16.0.h),
            detailWithdraw(Icons.attach_money, "WINNING"),
          ],
        ),
      ),
    );
  }

  Widget detailRow(icon, name) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                icon,
                size: 25,
              ).paddingRight(16.0.w),
              Text(name.toUpperCase())
            ],
          ),
          Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text("Rs 0".toUpperCase()).paddingRight(16.0.w),
                /*Icon(Icons.info_outline,size: 25,),*/
              ]),
        ],
      ),
    );
  }

  Widget detailWithdraw(icon, name) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Icon(
                icon,
                size: 25,
              ).paddingRight(16.0.w),
              Text(name.toUpperCase())
            ],
          ),
          Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                InkWell(
                  onTap: (){
                    Navigator.of(context).pushNamed(WithDrawScreen.routeName);
                  },
                  child
                      : Container(
                    height: 28,
                    width: 90,
                    decoration: BoxDecoration(
                        color: Colors.deepOrange,
                        borderRadius: BorderRadius.circular(8)),
                    child: Center(
                      child: Text(
                        "Withdraw",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ).paddingRight(16.0.w),
                ),
                Text("Rs 0".toUpperCase()),
                /*Icon(Icons.info_outline,size: 25,),*/
              ]),
        ],
      ),
    );
  }

  Widget transactionCard() {
    return InkWell(
      onTap: (){
        Navigator.pushNamed(
            context, TransactionScreen.routeName);
      },
      child: Card(
        elevation: 8.0,
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.track_changes,
                        size: 25,
                      ).paddingRight(16.0.w),
                      Text("My Recent Transactions".toUpperCase()),
                    ],
                  ),
                  Align(
                    alignment: Alignment.bottomRight,
                    child: Icon(
                      Icons.arrow_forward,
                      size: 25,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget getBottomContainer() {
    return Container(
      child: GridView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: 3,
          gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
             childAspectRatio: 4/3.5
          ),
          itemBuilder: (context, position) {
            if (position == 0) {
              return verificationCard("MOBILE & EMAIL", icon: Icons.mobile_screen_share);
            } else if (position == 1) {
              return verificationCard("PAN CARD", icon: Icons.credit_card);
            } else if (position == 2) {
              return verificationCard("WITHDRAW", icon: Icons.line_weight);
            } else {
              return verificationCard("nothing");
            }
          }),
    );
  }

  Widget verificationCard(text, {icon}) {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
      elevation: 8.0,
      child: Center(
        child: Container(
          width: MediaQuery.of(context).size.width * 0.45,
          padding: EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: EdgeInsets.all(12.0),
                decoration: BoxDecoration(
                    color: Colors.black,
                    borderRadius: BorderRadius.all(Radius.circular(12.0))),
                child: Icon(
                  icon,
                  size: 30,
                  color: Colors.white,
                ),
              ).paddingBottom(12.0.h),
              Text(
                text,
                textAlign: TextAlign.center,
                style:
                    TextStyle(fontSize: 18.0.sp, fontWeight: FontWeight.w800),
              ).paddingBottom(6.0.h),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(
                    Icons.verified_user,
                    color: Colors.deepOrange,
                  ),
                  Text(
                    "Verified",
                    style: TextStyle(color: Colors.green),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

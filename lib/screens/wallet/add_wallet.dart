import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:flutter/material.dart';

class AddCoinScreen extends BaseStatefulWidget {
  static const routeName = '/addcoin';

  @override
  _AddCoinScreenState createState() => _AddCoinScreenState();
}

class _AddCoinScreenState extends BaseState<AddCoinScreen>
    with BaseStatefulScreen {
  _AddCoinScreenState() : super(defaultPadding: false);

  @override
  Widget buildBody(BuildContext context) {
    return Container(
      color: Colors.white,
      width: MediaQuery.of(context).size.width,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Image.asset("assets/images/ic_wallet.png", height: 150, width: 150),
          getTextField(context, TextEditingController(), "Amount"),
          _addCoinButton()
        ],
      ),
    );
  }

  Widget getTextField(context, controller, String hint) => SizedBox(
    height: 45,
    width: MediaQuery.of(context).size.width * 0.50,
    child: TextField(
      autofocus: false,
      controller: controller,
      cursorColor: Colors.white,
      textAlignVertical: TextAlignVertical.center,
      style: TextStyle(fontSize: 16.0, color: Colors.white),
      decoration: InputDecoration(
        filled: true,
        floatingLabelBehavior: FloatingLabelBehavior.never,
        fillColor: Colors.yellow,
        hintText: hint,
        hintStyle: TextStyle(color: Colors.white),
        contentPadding: EdgeInsets.only(left: 14.0, right: 14),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.yellowAccent,
          ),
          borderRadius: BorderRadius.circular(12),
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.yellowAccent),
          borderRadius: BorderRadius.circular(12),
        ),
      ),
    ),
  );

  @override
  String screenName() {
    return "Add Coins";
  }

  Widget _addCoinButton() {
    return Container(
        margin: EdgeInsets.only(bottom: 20),
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        decoration: BoxDecoration(
            color: Colors.black,
            borderRadius: BorderRadius.all(Radius.circular(15))),
        child: Wrap(
          children: <Widget>[
            Transform.rotate(
              angle: 70,
              child: Icon(
                Icons.swap_calls,
                color: Colors.white,
              ),
            ),
            SizedBox(width: 10),
            Text(
              "Add Coins",
              style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w800,
                  color: Colors.white),
            )
          ],
        ));
  }
}

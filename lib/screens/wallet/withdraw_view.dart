import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/utils/responsive/flutter_screenutil.dart';
import 'package:ekojpubg/widgets/rounded_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/material.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';

class WithDrawScreen extends BaseStatefulWidget {
  static const routeName = '/withdraw';
  @override
  _WithDrawScreenState createState() => _WithDrawScreenState();
}

class _WithDrawScreenState extends BaseState<WithDrawScreen>
    with BaseStatefulScreen {
  _WithDrawScreenState() : super(defaultPadding: false);

  String _radioValue;

  @override
  String screenName() => "WITHDRAW";

  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);
    return SingleChildScrollView(
      child: Container(
        margin: EdgeInsets.only(top: 16.0.h),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            winningBalanceCard(),
            Text(
              "Withdrawal Method",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 24.sp,
                  fontWeight: FontWeight.w800),
            ).paddingAll(top: 24.0.h, left: 16.0.w),
            Container(margin: EdgeInsets.all(12.0), child: bankDetailsCard()),
            amountCards(),
            Text("Minimum Rs 25 and Maximum Rs 100000 allowed per day.",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey,
              fontSize: 24.0.sp
            ),).paddingAll(top: 24.0.h,bottom: 16.0.h,left: 32.0.w,right: 32.0.w),
            Container(
              width: MediaQuery.of(context).size.width,
              margin: EdgeInsets.only(left: 32.0.w,right: 32.0.w,top:32.0.h,bottom: 32.0.h),
              child: RoundedCornerButton((){},"WITHDRAW NOW"),
            )
          ],
        ),
      ),
    );
  }

  Widget winningBalanceCard() {
    return Card(
      elevation: 8.0,
      child: Container(
        padding: EdgeInsets.all(16.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(
                  Icons.cloud_download,
                  size: 25,
                ).paddingRight(16.0.w),
                Text(
                  "Your Winning Balance",
                  style: TextStyle(color: Colors.black),
                ),
              ],
            ),
            Align(
              alignment: Alignment.bottomRight,
              child: Text("Rs 0.00".toUpperCase()),
            )
          ],
        ),
      ),
    );
  }

  Widget bankDetailsCard() {
    return Card(
      elevation: 8.0,
      child: Container(
        padding: EdgeInsets.all(12.0),
        child: Column(
          children: <Widget>[
            bankDetails(),
            Divider(
              thickness: 1.2,
              color: Colors.black,
            ),
            upiDetails()
          ],
        ),
      ),
    );
  }

  Widget bankDetails() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
                    padding: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                        color: Colors.orangeAccent,
                        borderRadius: BorderRadius.all(Radius.circular(8.0))),
                    child: Icon(Icons.account_balance))
                .paddingRight(16.0.w),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Bank Account",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 24.sp,
                      fontWeight: FontWeight.w800),
                ).paddingBottom(8.0.h),
                Text(
                  "Name - Kundan Kumar",
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 18.sp,
                  ),
                ).paddingBottom(4.0.h),
                Text(
                  "Bank Account - STATE BANK OF INDIA",
                  style: TextStyle(
                      color: Colors.grey,
                      fontSize: 16.sp,
                      fontWeight: FontWeight.w800),
                ).paddingBottom(4.0.h),
                Text(
                  "A/C -  344567288890",
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 18.sp,
                  ),
                ).paddingBottom(8.0.h),
              ],
            ),
          ],
        ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text("Linked",style: TextStyle(
                  color: Colors.orangeAccent,
                  fontSize: 20.sp,
                  fontWeight: FontWeight.w800,
                ),),
                Radio(
                  activeColor: Colors.orangeAccent,
                  focusColor: Colors.orangeAccent,
                  value: _radioValue,
                )
              ],
            )
      ],
    );
  }

  Widget  upiDetails(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Row(
 children: <Widget>[
   Container(
       padding: EdgeInsets.all(8.0),
       decoration: BoxDecoration(
           color: Colors.orangeAccent,
           borderRadius: BorderRadius.all(Radius.circular(8.0))),
       child: Icon(Icons.payment))
       .paddingRight(16.0.w),
   Text(
     "UPI",
     style: TextStyle(
         color: Colors.black,
         fontSize: 24.sp,
         fontWeight: FontWeight.w800),
   )
 ],
        ),
        Text("Link UPI",style: TextStyle(
        color: Colors.black,
          fontSize: 20.0.sp,
          fontWeight: FontWeight.w800
        ),).paddingRight(16.0)
      ],
    );
  }

  Widget amountCards(){
    return Container(
      margin: EdgeInsets.only(left: 8.0.w,right: 8.0.w,top:8.0.h),
      child: Card(
          elevation: 8.0,
          child: Container(
            padding: EdgeInsets.all(16.0),
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
              Text("Amount",style: TextStyle(
              color: Colors.black,
              fontSize: 24.0.sp,
              fontWeight: FontWeight.w800
              ),).paddingBottom(6.0.h),
                Text("Rs 400",style: TextStyle(
                  color: Colors.blue,
                  fontWeight: FontWeight.w800,
                  fontSize: 24.0.sp
                ),),

              ],
            ),
          )),
    );
  }
}

import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/model/Matches.dart';
import 'package:ekojpubg/model/drawer/MatchJoinedTeamResponse.dart';
import 'package:ekojpubg/model/network/MatchSlotsResponse.dart';
import 'package:ekojpubg/screens/mobilescrims/service/MatchJoinedTeamService.dart';
import 'package:ekojpubg/screens/mobilescrims/service/PaidMatchSlotsService.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/utils/responsive/screenutil.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';
import 'package:ekojpubg/widgets/ProgressDialogType.dart';
import 'package:ekojpubg/widgets/TeamItem.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class SlotPubgScrimScreen extends BaseStatefulWidget {
  static const routeName = '/slot_pubg';
  @override
  _SlotPubgScrimState createState() => _SlotPubgScrimState();
}

class _SlotPubgScrimState extends BaseState<SlotPubgScrimScreen>
    with BaseStatefulScreen {
  final matchesList = [
    MatchModel("1", true),
    MatchModel("2", false),
    MatchModel("3", false),
    MatchModel("4", false)
  ];

  MatchRounds selectedMatch;
  ProgressDialog progressDialog;
  PaidMatchSlotsService paidMatchSlotsService;
  MatchJoinedTeamService matchJoinedTeamService;
  List<MatchSlots> matches;
  List<MatchRounds> matchRounds;
 List<Team> teamList;
 List<MemberIds> memberList;

  @override
  void initState() {
    super.initState();
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    paidMatchSlotsService = PaidMatchSlotsService();
    matchJoinedTeamService = MatchJoinedTeamService();
    getPaidMatches();
    getJoinedMatchTeams();
  }

  @override
  String screenName() => "PUBG MOBILE Scrims";


  void getPaidMatches() async {
    final userId = await SharedPreferenceUtil.getUserid();
    await progressDialog.show();
    paidMatchSlotsService.getPaidMatchesSlots("8", "8").then((value) {
      if (value != null &&
          value.status == "success" &&
          value.data != null &&
          value.data.isNotEmpty) {
        setState(() {
          matches=value.data;
          if(matches.length>0 && matches[0].matchRounds!=null && matches[0].matchRounds.length>0){
            matchRounds=matches[0].matchRounds;
            matchRounds[0].selected=true;
            selectedMatch = matchRounds[0];
          }
        });

      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  void getJoinedMatchTeams() async{
    await progressDialog.show();
    matchJoinedTeamService.getJoinedMatchTeams("9").then((value) {
      if (value != null &&
          value.status == "success" &&
          value.data != null &&
          value.data.isNotEmpty) {
        setState(() {
          teamList=value.data;

        });

      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }
  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);
    return SingleChildScrollView(
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "Upcoming Matches",
              style: TextStyle(color: Colors.red, fontSize: 16.0.sp),
            ).paddingAll(top: 16.0.h, left: 24.w),
           matches!=null ? ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                physics: NeverScrollableScrollPhysics(),
                itemCount: matchRounds.length,
                itemBuilder: (context, pos) {
                  return InkWell(
                    child: MatchItem(matchRounds[pos]),
                    onTap: () {
                      if (selectedMatch != matchRounds[pos]) {
                        selectedMatch.selected = false;
                        selectedMatch = matchRounds[pos]..selected = true;
                        setState(() {});
                      }
                    },
                  );
                }):Center(child: Text("No Result Found")),
            Text(
              "My Team",
              style: TextStyle(color: Colors.black, fontSize: 16.0.sp),
            ).paddingAll(top: 30.0.h, left: 24.w),
          //  TeamItem("Team 15").paddingAll(left: 16, right: 16),
            teamList!=null && teamList.length>0?ListView.builder(
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: teamList.length,
                itemBuilder: (context, pos) {
                  return TeamItem(" ${pos + 1}",team:teamList[pos]).paddingAll(top :pos % 3 ==0 ? 30 : 8, left: 16, right: 16);
                }):Center(child: Text("No Value Found"))
          ],
        ),
      ),
    ).noScrollEffect();
  }


}

class MatchItem extends StatefulWidget {
  final MatchRounds model;

  MatchItem(this.model);

  @override
  _MatchItemState createState() => _MatchItemState();
}

class _MatchItemState extends State<MatchItem> {
  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.centerLeft,
      child: AnimatedContainer(
        duration: Duration(milliseconds: 500),
        margin: EdgeInsets.only(
            left: 16.0.w, right: 16.0.w, top: 8.0.h, bottom: 8.0.h),
        padding: EdgeInsets.only(
            left: 32.0.w, right: 32.0.w, top: 16.0.h, bottom: 16.0.h),
        width: widget.model.selected
            ? MediaQuery.of(context).size.width
            : MediaQuery.of(context).size.width * 0.65,
        decoration: BoxDecoration(
            color: widget.model.selected ? Colors.black : Color(0xFFD5D5D5),
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "Match ${this.widget.model.match} ${this.widget.model.roundName.toUpperCase()}",
                  style: TextStyle(
                      fontSize: 16.sp,
                      color:
                          widget.model.selected ? Colors.white : Colors.black),
                ),
                Text("Starting Time - ${this.widget.model.matchDatetime.split("T")[1].split("+")[0]}",
                        style: TextStyle(
                            fontSize: 12.0.sp,
                            color: widget.model.selected
                                ? Colors.white
                                : Colors.black))
                    .paddingAll(top: 8.0.h),
              ],
            ),
            Text(
              "Date ${this.widget.model.matchDatetime.split("T")[0]}",
              style: TextStyle(
                  color: widget.model.selected ? Colors.white : Colors.black,
                  fontSize: 12.sp),
            )
          ],
        ),
      ),
    );
  }
}



import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/network/MatchSlotsResponse.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class PaidMatchSlotsService{
  Future<PaidMatchSlotsResponse> getPaidMatchesSlots(userId, matchId) async {

    //?user_id=8&game_id=1
    ApiResponse apiResponse =
    await ApiHitter.getApiResponse(AppConstants.UPCOMING, queryParameters: {
      "user_id" : userId,
      "match_id" : matchId
    });

    try {
      return apiResponse.response != null
          ? PaidMatchSlotsResponse.fromJson(apiResponse.response.data)
          : (CommonResponse()..Message = apiResponse.msg);
    } catch (e) {
      print(e..toString());
      return null;
    }
  }
}
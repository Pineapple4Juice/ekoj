
import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/drawer/MatchJoinedTeamResponse.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class MatchJoinedTeamService{
  Future<MatchJoinedTeamResponse> getJoinedMatchTeams(matchId) async {

    //?user_id=8&game_id=1
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.MATCHJOINEDTEAM, data: {
      "match_id" : matchId
    });

    try {
      return apiResponse.response != null
          ? MatchJoinedTeamResponse.fromJson(apiResponse.response.data)
          : (CommonResponse()..Message = apiResponse.msg);
    } catch (e) {
      print(e..toString());
      return null;
    }
  }
}

import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/model/network/GameDetailResponse.dart';
import 'package:ekojpubg/screens/mobilescrims/slot_pubg_scrim.dart';
import 'package:ekojpubg/utils/responsive/screenutil.dart';
import 'package:ekojpubg/widgets/rounded_button.dart';
import 'package:ekojpubg/widgets/small_rounded_btn.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';


class PubgMobileScrims extends BaseStatefulWidget{
  static const routeName = '/pubg_mobileScrims';
  @override
  _PubgMobileScrimsState createState() =>_PubgMobileScrimsState();

}


class _PubgMobileScrimsState extends BaseState<PubgMobileScrims> with BaseStatefulScreen{


  _PubgMobileScrimsState():super(defaultPadding:false);

  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);
    final List<Data> allScrims = ModalRoute.of(context).settings.arguments as List<Data> ;
   return SingleChildScrollView(
     child: Container(
       color: Colors.white,
       //padding: EdgeInsets.all(16.0),
       child:Column(
         crossAxisAlignment: CrossAxisAlignment.start,
         mainAxisAlignment: MainAxisAlignment.start,
         children: <Widget>[
           Text("Choose Your Matches From Selected TimeZones",
           style: TextStyle(
             color: Colors.red,
             fontSize: 16.0.sp
           ),).paddingAll(top:16.0.h,left: 24.w),
           Container(
             child: GridView.builder(

                 physics: ClampingScrollPhysics(),
               shrinkWrap: true,
                 itemCount: 6,
                 gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
                 crossAxisCount: 2,
                   childAspectRatio: 3.5,), itemBuilder: (context,pos){
                   return Container(

                       margin: EdgeInsets.all(8.0),
                       width: MediaQuery.of(context).size.width*0.5,
                       decoration:BoxDecoration(
                        color:Color(0xFFD5D5D5),
                         borderRadius: BorderRadius.all(Radius.circular(32.0))
                       ) ,
                       child: Center(
                         child: Text(
                           "12 PM - 3 PM",
                           style: TextStyle(
                             fontWeight: FontWeight.w800,
                             color: Colors.black
                           ),
                         ),
                       ));
             }),
           ),
           Divider(thickness: 1.0,
           color: Colors.grey,),
           ListView.builder(
             shrinkWrap:true,
             physics: NeverScrollableScrollPhysics(),
             itemCount: 10,
             itemBuilder: (context,pos){
             return matchScrimItem();
           },
           )
         ],
       ) ,
     ),
   );
  }

  @override
  String screenName() =>"PUBG MOBILE Scrims";

  Widget matchScrimItem(){
    return Container(
      padding: EdgeInsets.only(left:16.0,right:16.0,top: 8.0),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[

                  Text("Match #8805 Squad",
                  style: TextStyle(
                    fontSize: 28.sp,
                    color: Colors.black
                  ),),
                  Text("Starting Time - 6:00 PM",
                      style: TextStyle(
                          fontSize: 20.0.sp,
                          color: Colors.black
                      )).paddingAll(top: 12.0.h,bottom: 24.0.h),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Align(
                        alignment: Alignment.topCenter,
                        child: RichText(
                          textAlign: TextAlign.center,
                          text: new TextSpan(
                            // Note: Styles for TextSpans must be explicitly defined.
                            // Child text spans will inherit styles from parent
                            style: new TextStyle(
                              fontSize: 24.0.sp,
                              color: Colors.black,


                            ),
                            children: <TextSpan>[
                              new TextSpan(text: '\u2022 ' ,
                                  style: TextStyle(
                                  fontSize: 28.0.sp,
                                  color: Colors.red
                              )),
                              new TextSpan(text: 'LIVE',style: TextStyle(
                                  letterSpacing: 1.2,
                                  fontSize: 18.0.sp,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.grey)),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(width:14.w ,),
                      Image.asset(
                        "assets/images/youtube.png",
                        width: 35.w,
                        height: 25.h,
                        fit: BoxFit.fill,
                      ),
                      SizedBox(width:8.w ,),
                      Image.asset(
                        "assets/images/facebook_circled.png",
                        width: 30.w,
                        height: 30.h,


                      ),

                    ],
                  )
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Text(
                    "Date 06/06/2020",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.sp
                    ),
                  ).paddingBottom(4.0.h),
                  SmallRoundedCornerButton(
                          (){
                            Navigator.pushNamed(context, SlotPubgScrimScreen.routeName);
                          },"Book Slot"
                  )
                ],
              )
            ],
          ),
          Divider(
            thickness: 1.2,
            color: Colors.black,
          )
        ],
      ),

    );
  }

}
import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/commons/FutureConsumer.dart';
import 'package:ekojpubg/model/drawer/CustomerSupportResponse.dart';
import 'package:ekojpubg/screens/customer_support/service/CustomSupportService.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class CustomerSupportScreen extends BaseStatefulWidget {
  static const routeName = '/customer';

  @override
  _CustomerSupportScreenState createState() => _CustomerSupportScreenState();
}

class _CustomerSupportScreenState extends BaseState<CustomerSupportScreen>
    with BaseStatefulScreen {
  _CustomerSupportScreenState() : super(defaultPadding: false);

  @override
  Widget buildBody(BuildContext context) {
    return FutureBuilder(
      future: CustomSupportService().getCustomSupport(),
      builder: (context, snapshot) {
        return streamConsumer(snapshot, () {
          var supportList = (snapshot.data as CustomSupportResponse).data;
          return supportList.length != null ?
          Container(
            color: Colors.white,
            padding: EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text("Reach us on social media platforms",
                    style: TextStyle(color: Colors.red)),
                GridView.builder(
                  itemCount: supportList.length,
                    shrinkWrap: true,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 16,
                    mainAxisSpacing: 16,
                    childAspectRatio: 4 / 3.9), itemBuilder: (context,position){
                      return SocialCard("assets/images/ic_logo_instagram.png",supportList[position].socialMediaName).paddingBottom(16);
                }).paddingAll(top: 20, left: 16, right: 16),

               SizedBox(height: 20,)
              ],
            ),
          )
          :Container(child: Center(child: Text("No value find."),),);
          });
      },

    );
  }

  @override
  String screenName() {
    return "Customer Support";
  }
}

class SocialCard extends StatelessWidget {
  final String logo;
  final String name;

  SocialCard(this.logo, this.name);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      width: 70,
      decoration: BoxDecoration(
          color: Colors.white, borderRadius: BorderRadius.circular(12)),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Image.asset(logo, height: 40, width: 40, fit: BoxFit.cover),
          Text(name, style: TextStyle(color: Colors.black)).paddingTop(2)
        ],
      ),
    ).elevation(12);
  }
}



import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/model/drawer/CustomerSupportResponse.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class CustomSupportService {
   Future<CustomSupportResponse> getCustomSupport() async{
     var userId= await SharedPreferenceUtil.getUserid();
     ApiResponse apiResponse= await ApiHitter.getApiResponse(AppConstants.CUSTOMER_SUPPORT);

     try{
       return apiResponse.response != null ? CustomSupportResponse.fromJson(
           apiResponse.response.data) : (CommonResponse()
         ..Message = apiResponse.msg);
     }catch(e){
       print(e..toString());
       return null;
     }
}
}
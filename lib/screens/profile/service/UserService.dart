

import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/UserData.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class UserService{
  Future<UserData> getProfileData(userId) async {

    //?user_id=8&game_id=1
    ApiResponse apiResponse =
    await ApiHitter.getApiResponse(AppConstants.GETPROFILE, queryParameters: {
      "user_id" : userId
    });

    try {
      return apiResponse.response != null
          ? UserData.fromJson(apiResponse.response.data)
          : (CommonResponse()
        ..Message = apiResponse.msg);
    } catch (e) {
      print(e..toString());
      return null;
    }
  }
}
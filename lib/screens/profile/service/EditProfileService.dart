

import 'package:dio/dio.dart';
import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/UserData.dart';
import 'package:ekojpubg/model/drawer/EditProfileResponse.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';
import 'package:ekojpubg/screens/profile/constants/ProfileParams.dart';

class EditProfileService{
  Future<EditProfileResponse> editProfile(ProfileParams profileParams, Data profileData) async {

    //?user_id=8&game_id=1
    ApiResponse apiResponse =
    await ApiHitter.getPostApiResponse(AppConstants.EDITPROFILE, formData: await uploadImage(profileParams, profileData));
    try {
      return apiResponse.response != null
          ? EditProfileResponse.fromJson(apiResponse.response.data)
          : (CommonResponse()
        ..Message = apiResponse.msg);
    } catch (e) {
      print(e..toString());
      return null;
    }
  }

  Future<FormData> uploadImage(ProfileParams profileParams, Data data) async {
    FormData formData = FormData.fromMap({
    "first_name":profileParams.first_name,
      "middle_name":profileParams.middle_name,
      "last_name":profileParams.last_name,
      data.phoneNumber != profileParams.phone_number ? "phone_number" : "": data.phoneNumber != profileParams.phone_number ? profileParams.phone_number : "",
      "user_id":profileParams.user_id,
      profileParams.profile_pic == null ? "" : "profile_pic": profileParams.profile_pic == null ? "" : await MultipartFile.fromFile(profileParams.profile_pic.path ,filename:profileParams.profile_pic.path.split('/').last),
      "dob":profileParams.dob,
      "address":profileParams.address
    });

    return formData;
  }
}
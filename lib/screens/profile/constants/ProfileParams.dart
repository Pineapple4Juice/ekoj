
import 'dart:io';

class ProfileParams{
  String first_name;
  String last_name;
  String middle_name="";
  String phone_number;
  String user_id;
  File profile_pic;
  String dob;
  String address;
}
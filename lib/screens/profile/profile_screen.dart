import 'dart:io';

import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/model/UserData.dart';
import 'package:ekojpubg/model/drawer/EditProfileResponse.dart';
import 'package:ekojpubg/screens/profile/constants/ProfileParams.dart';
import 'package:ekojpubg/screens/profile/service/EditProfileService.dart';
import 'package:ekojpubg/screens/profile/service/UserService.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';
import 'package:ekojpubg/widgets/ProgressDialogType.dart' as PD;
import 'package:image_picker/image_picker.dart';

import 'package:flutter/material.dart';

class ProfileScreen extends BaseStatefulWidget {
  static const routeName = '/profile';

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends BaseState<ProfileScreen>
    with BaseStatefulScreen {
  _ProfileScreenState() : super(defaultPadding: false);

  PD.ProgressDialog progressDialog;
  UserService userService;
  EditProfileService editProfileService;
  Data profileData;
  ProfileData editProfileResponse;
  PickedFile _imageFile; //here we gonna store our image.
  dynamic _pickImageError;
  final ImagePicker _picker = ImagePicker();
  TextEditingController firstNameController;
  TextEditingController lastNameController;
  TextEditingController phoneNumberController;
  TextEditingController dobController;
  TextEditingController emailController;
  TextEditingController addressController;



  @override
  String screenName() => "Profile";

  @override
  void initState() {
    super.initState();
    userService = UserService();
    editProfileService=EditProfileService();
    getProfileData();
  }

  void getProfileData() async{
    final userId = await SharedPreferenceUtil.getUserid();
    await progressDialog.show();
    userService.getProfileData("8").then((value) {
      if (value != null &&
          value.status == "success") {
        setState(() {
          profileData=value.data;
          initialControllerValue();
        });

      //  Navigator.pushNamed(context, SlotPubgScrimScreen.routeName, arguments: paidMatches);
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  Future getImagefromGallery() async {
    try{
    var image = await _picker.getImage(source: ImageSource.gallery);
    setState(() {
      this._imageFile=image;
    });
    }catch(e){
  _pickImageError =e;
    }
  }


  void initialControllerValue(){
    profileData!=null && profileData.firstName!=null? firstNameController=TextEditingController(text: profileData.firstName):TextEditingController();
    profileData!=null && profileData.lastName!=null ? lastNameController=TextEditingController(text: profileData.lastName):TextEditingController();
    profileData!=null && profileData.phoneNumber!=null ? phoneNumberController=TextEditingController(text: profileData.phoneNumber):TextEditingController();
    profileData!=null && profileData.dob!=null ? dobController=TextEditingController(text: profileData.dob):TextEditingController();
    profileData!=null && profileData.email!=null ? emailController=TextEditingController(text: profileData.email):TextEditingController();
    profileData!=null && profileData.address!=null ? addressController=TextEditingController(text: profileData.address):TextEditingController();
  }

  void updateProfileData(ProfileParams profileParams,context) async{
    final userId = await SharedPreferenceUtil.getUserid();
    await progressDialog.show();
    editProfileService.editProfile(profileParams, profileData).then((value) {
      if (value != null &&
          value.status == "success") {
        setState(() {
          editProfileResponse=value.data;
          print("updated");
        });

        //  Navigator.pushNamed(context, SlotPubgScrimScreen.routeName, arguments: paidMatches);
      }
    }).whenComplete(() async {
    await progressDialog.hide();
    });
  }

  ProfileParams getInputData(){
    ProfileParams profileParams=ProfileParams();
    profileParams.first_name=firstNameController.text.toString();
    profileParams.last_name=lastNameController.text.toString();
    profileParams.phone_number=phoneNumberController.text.toString();
    profileParams.user_id="8";
    profileParams.dob=dobController.text.toString();
    profileParams.address=addressController.text.toString();
    profileParams.profile_pic=_imageFile!=null?File(_imageFile.path):null;
    return profileParams;
  }
  @override
  Widget buildBody(BuildContext context) {
    progressDialog = PD.ProgressDialog(context,
        type: PD.ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    return profileData!=null?Container(
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              InkWell(
                onTap: (){
                  getImagefromGallery();
                },
                child: _imageFile!=null?Image.file(File(_imageFile.path),width: 170.w,height: 170.w,fit: BoxFit.cover,).
                 paddingAll(left: 16, top: 16, bottom: 16, right: 16)
                    :Container(
                  height: 170.w,
                  width: 170.w,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: NetworkImage(AppConstants.domain +
                          profileData.profilePic),fit: BoxFit.cover
                    ),
                  ),
                ).paddingAll(left: 16, top: 16, bottom: 16, right: 16),
              ),

              /*Image.asset(
                'assets/images/pubg_cover.jpg',
                height: 170.w,
                width: 170.w,
                fit: BoxFit.cover,
              ).paddingAll(left: 16, top: 16, bottom: 16, right: 16),*/
              Text(
                profileData.firstName.toUpperCase() + " " +profileData.middleName.toUpperCase() +" " + profileData.lastName.toUpperCase(),
                style: TextStyle(
                    fontSize: 30.sp,
                    color: Colors.black,
                    fontWeight: FontWeight.w500),
              ).paddingBottom(16.0.h)
            ],
          ),
          Container(
            height: 0.01,
            color: Colors.grey,
          ).elevation(0, elevation: 3),
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(left: 16, right: 16),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    getTextField(context, firstNameController, "First Name"),
                    SizedBox(height: 16.h,),
                    getTextField(context, lastNameController, "Last Name"),
                    SizedBox(height: 16.h,),
                    getTextField(context, phoneNumberController, "Phone Number"),
                    SizedBox(height: 16.h,),
                    getTextField(context, dobController, "Date of Birth"),
                    SizedBox(height: 16.h,),
                    getTextField(context, emailController,"Email"),
                    SizedBox(height: 16.h,),
                    getTextField(context, addressController, "Address"),
                    Container(
                        padding: EdgeInsets.all(8.h),
                        margin: EdgeInsets.only(top: 16.h),
                        decoration: BoxDecoration(
                            color: Colors.grey.withAlpha(100),
                            borderRadius: BorderRadius.circular(8)),
                        child: GridView.builder(
                            shrinkWrap: true,
                            gridDelegate:
                            SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 3,
                                mainAxisSpacing: 4,
                                crossAxisSpacing: 4,
                                childAspectRatio: 4 / 1),
                            itemCount: 5,
                            itemBuilder: (context, pos) {
                              return Row(
                                  mainAxisAlignment: MainAxisAlignment
                                      .spaceAround,
                                  children: <Widget>[
                              Expanded(
                              child: Text("PUBG Lite").paddingLeft(8.w)),
                              InkWell(
                                onTap: () {
                                  showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return SimpleDialog(
                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                                            title: Text("PUBG Lite"),
                                            children: <Widget>[
                                              SizedBox(height: 16),
                                              getTextField(context, TextEditingController(), "IGN").paddingAll(left: 8, right: 8),
                                              SizedBox(height: 16),
                                              getTextField(context, TextEditingController(), "ID").paddingAll(left: 8, right: 8),
                                              SizedBox(height: 16),
                                            ]..add(Container(
                                              margin: EdgeInsets.only(top: 16),
                                              child: Center(
                                                child: SizedBox(
                                                  height: 42,
                                                  width: 100,
                                                  child: RaisedButton(
                                                    elevation: 5.0,
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                    color: Colors.black,
                                                    shape: new RoundedRectangleBorder(
                                                      borderRadius: new BorderRadius.circular(14.0),
                                                    ),
                                                    child: Text(
                                                      'SAVE',
                                                      style: TextStyle(
                                                        fontWeight: FontWeight.w800,
                                                        fontSize: 14.0,
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )));
                                      });

                                },
                              child: Image.asset(
                              "assets/images/ic_add_circle.png",
                              height: 20,
                              width: 20,
                              ).paddingAll(left: 8.w, right: 16.w),
                              ),
                              ],
                              );
                            })),
                    SizedBox(height: 20,),
                    SizedBox(
                      width: 220.w,
                      height: 42.h,
                      child: RaisedButton(
                        //splashColor: Colors.lightBlue,
                        onPressed: () {
                          var profileParams= getInputData();
                          updateProfileData(profileParams,context);
                        },
                        color: Color(0xFF9BADD7),
                        child: Text("Save Changes",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 22.sp,
                                fontWeight: FontWeight.bold)),
                        //disabledColor: Colors.lightBlue,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(24)),
                      ),
                    ).elevation(24.0, elevation: 8.0),
                    SizedBox(height: 20,),

                  ],
                ),
              ),
            ),
          )
        ],
      ),
    ):Center(child: Text("No Data Found."),);
  }

  Widget getField(name) {
    return Container(
      margin: EdgeInsets.only(top: 16.h),
      height: 48.h,
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.grey.withAlpha(100),
          borderRadius: BorderRadius.circular(8)),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text(
          name,
          style: TextStyle(color: Colors.black, fontSize: 20.sp),
        ).paddingLeft(16.w),
      ),
    );
  }
}


Widget getTextField(context, controller, String hint, {bool pass = false}) =>
    SizedBox(
      height: 48.h,
      child: TextField(
        obscureText: pass,
        controller: controller,
        cursorColor: Colors.white,
        textAlignVertical: TextAlignVertical.center,
        style: TextStyle(
            fontSize: 14.0, color: Colors.black87, letterSpacing: 1.2),
        decoration: InputDecoration(
          filled: true,
          floatingLabelBehavior: FloatingLabelBehavior.never,
          fillColor: Color(0xFFEAEAEA),
          hintText: hint,
          hintStyle: TextStyle(color: Colors.black87),
          contentPadding: EdgeInsets.only(left: 14.0, right: 14),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(8.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
      ),
    );


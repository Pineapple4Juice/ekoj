import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/model/network/GameDetailResponse.dart';
import 'package:ekojpubg/screens/gameDetail/GameItemDetailScreen.dart';
import 'package:ekojpubg/screens/gameDetail/service/GameDetailService.dart';
import 'package:ekojpubg/screens/mobilescrims/pubg_mobile_scrims.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/widgets/ProgressDialogType.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GameDetailScreen extends BaseStatefulWidget {
  static const routeName = '/game_detail';

  @override
  _GameDetailScreenState createState() => _GameDetailScreenState();
}

class _GameDetailScreenState extends BaseState<GameDetailScreen>
    with BaseStatefulScreen {
  _GameDetailScreenState() : super(defaultPadding: false);
  ProgressDialog progressDialog;
  GameDetailService gameDetailService;
  List<Data> matchList;
  List<Data> paidMatches;
  List<Data> scrimMatches;

  @override
  void initState() {
    super.initState();
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    gameDetailService = GameDetailService();
    getGameData();
  }

  @override
  Widget buildBody(BuildContext context) {
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            scrimMatches != null ? Container(
              height: 38,
              width: 150,
              decoration: BoxDecoration(
                  color: Colors.deepPurpleAccent,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(14),
                      topRight: Radius.circular(30))),
              child: Center(
                  child: Text(
                "Scrims",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.amberAccent,
                    fontWeight: FontWeight.bold),
              )),
            ) : SizedBox(),
            scrimMatches != null ? InkWell(
              onTap: () {
                Navigator.of(context).pushNamed(PubgMobileScrims.routeName,arguments: scrimMatches);
              },
              child: Container(
                height: MediaQuery.of(context).size.height * 0.30,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.deepPurpleAccent,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.zero,
                        topRight: Radius.circular(12),
                        bottomLeft: Radius.circular(12),
                        bottomRight: Radius.circular(12)),
                    image: DecorationImage(
                        image: AssetImage("assets/images/pubg_cover.jpg"),
                        fit: BoxFit.cover)),
              ),
            ) : SizedBox(),
            paidMatches != null ? Container(
              height: 38,
              width: 150,
              decoration: BoxDecoration(
                  color: Color(0xFFE3E3E3),
                  borderRadius: BorderRadius.circular(24)),
              child: Center(
                  child: Text(
                "Paid Matches",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.deepPurpleAccent,
                    fontWeight: FontWeight.bold),
              )),
            ).elevation(24).paddingTop(16) : SizedBox(),
            paidMatches != null
                ? GridView.builder(
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: paidMatches.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        crossAxisSpacing: 8.0,
                        mainAxisSpacing: 8.0,
                        childAspectRatio: 12 / 10),
                    itemBuilder: (context, pos) {
                      return InkWell(
                          child: Container(
                            width: 200,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(12),
                                  image: DecorationImage(
                                      image: NetworkImage(AppConstants.domain +
                                          paidMatches[pos].cover), fit: BoxFit.cover))),
                          onTap: () {
                            Navigator.of(context)
                                .pushNamed(GameItemDetailScreen.routeName,arguments: paidMatches[pos]);
                          });
                    }).paddingTop(16)
                : SizedBox()
          ],
        ),
      ),
    );
  }

  final names = ["Solo", "Squad", "Duo", "Solo"];

  @override
  String screenName() {
    return "PUBG MOBILE";
  }

  void getGameData() async {
    final userId = await SharedPreferenceUtil.getUserid();
    await progressDialog.show();
    gameDetailService.getGameMatches("8", "1").then((value) {
      if (value != null &&
          value.status == "success" &&
          value.data != null &&
          value.data.isNotEmpty) {
        setState(() {
          matchList = value.data;
          paidMatches = value.data.where((element) => element.matchName != "scrims").toList();
          scrimMatches = value.data.where((element) => element.matchName == "scrims").toList();
        });
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }
}

class GameItem extends StatelessWidget {
  final String name;

  GameItem({this.name = "Solo"});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.black, borderRadius: BorderRadius.circular(12)),
      child: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height * 0.22,
            width: 200,
            decoration: BoxDecoration(
                color: Colors.deepPurple,
                borderRadius: BorderRadius.circular(12),
                image: DecorationImage(
                    image: AssetImage("assets/images/pubg_cover.jpg"),
                    fit: BoxFit.cover)),
          ).clip(WaveClipper()),
          Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(name,
                          style: TextStyle(
                              fontSize: 19,
                              fontWeight: FontWeight.bold,
                              color: Colors.deepOrange)),
                      Text("PUBG MOBILE",
                              style: TextStyle(
                                  fontSize: 8,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.white))
                          .paddingAll(top: 4, bottom: 4),
                    ],
                  ).paddingLeft(8),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Prize Pool - 5000",
                          style: TextStyle(
                              fontSize: 7,
                              fontWeight: FontWeight.w300,
                              color: Colors.white)),
                      Text("Entry Fees - 50",
                              style: TextStyle(
                                  fontSize: 7,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.white))
                          .paddingAll(top: 4),
                      Text("Per Kill - 45",
                              style: TextStyle(
                                  fontSize: 7,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.white))
                          .paddingAll(top: 4),
                      Text("Starting at 6:30 PM",
                              style: TextStyle(
                                  fontSize: 7,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.white))
                          .paddingAll(top: 4, bottom: 4),
                    ],
                  ).paddingRight(8)
                ],
              ))
        ],
      ),
    );
  }
}

class WaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height * 0.70);
    path.quadraticBezierTo(size.width * 0.25, size.height * 0.80,
        size.width * 0.55, size.height * 0.70);
    path.cubicTo(size.width * 0.65, size.height * 0.67, size.width * 0.80,
        size.height * 0.58, size.width * 0.90, size.height * 0.53);
    path.quadraticBezierTo(
        size.width * 0.95, size.height * 0.505, size.width, size.height * 0.50);
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

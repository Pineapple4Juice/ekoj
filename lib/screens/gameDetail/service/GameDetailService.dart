import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/network/GameDetailResponse.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class GameDetailService {
  Future<GameDetailResponse> getGameMatches(userId, gameId) async {

    //?user_id=8&game_id=1
    ApiResponse apiResponse =
        await ApiHitter.getApiResponse(AppConstants.UPCOMING, queryParameters: {
          "user_id" : userId,
          "game_id" : gameId
    });

    try {
      return apiResponse.response != null
          ? GameDetailResponse.fromJson(apiResponse.response.data)
          : (GameDetailResponse()..Message = apiResponse.msg);
    } catch (e) {
      print(e..toString());
      return null;
    }
  }
}

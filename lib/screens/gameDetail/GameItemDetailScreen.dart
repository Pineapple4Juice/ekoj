import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/model/network/GameDetailResponse.dart';
import 'package:ekojpubg/screens/bookSlotDetail/BookSlotDetailScreen.dart';
import 'package:ekojpubg/screens/gameDetail/service/JoinMatchService.dart';
import 'package:ekojpubg/screens/mobilescrims/slot_pubg_scrim.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/widgets/ProgressDialogType.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ekojpubg/utils/extensions/context.dart';

class GameItemDetailScreen extends BaseStatefulWidget {
  static const routeName = '/game_item_detail';

  @override
  _GameItemDetailScreenState createState() => _GameItemDetailScreenState();
}

class _GameItemDetailScreenState extends BaseState<GameItemDetailScreen>
    with BaseStatefulScreen {
  _GameItemDetailScreenState() : super(defaultPadding: false);

  ProgressDialog progressDialog;
  JoinMatchService joinMatchService;

  @override
  void initState() {
    super.initState();
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    joinMatchService = JoinMatchService();
  }

  void getJoinedMatchResponse(context,paidMatches) async {
    final userId = await SharedPreferenceUtil.getUserid();
    await progressDialog.show();
    joinMatchService.getJoinMatchStatus("8", "1").then((value) {
      if (value != null &&
          value.status == "success") {
        Navigator.pushNamed(context, SlotPubgScrimScreen.routeName, arguments: paidMatches);
      }else if(value != null &&
          value.status != "success"){
        context.showAlert(value.Message);
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  @override
  Widget buildBody(BuildContext context) {
    final Data paidMatches = ModalRoute.of(context).settings.arguments as Data;
    return Container(
      color: Colors.black,
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height*0.85,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: NetworkImage(
                        AppConstants.domain+ paidMatches.profile
                    ),
                    fit: BoxFit.fill
                  )
                ),
              ),
             /* Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Solo",
                          style: TextStyle(
                              fontSize: 38,
                              fontWeight: FontWeight.bold,
                              color: Colors.deepOrange)),
                      SizedBox(height: 8),
                      Container(color: Colors.grey, height: 0.7, width: 70)
                          .elevation(0, elevation: 4)
                          .paddingLeft(24),
                      Text("Map - Erangle",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.deepOrange))
                          .paddingTop(16),
                      Text("Mode - Tpp",
                              style: TextStyle(
                                  fontSize: 16,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.deepOrange))
                          .paddingTop(8),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.calendar_today),
                          Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text("Date",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.deepOrange)),
                                Text("26/6/2020",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.deepOrange)),
                              ]).paddingLeft(4)
                        ],
                      ).paddingTop(16.0),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.access_time),
                          Text("Time - 6:30 PM",
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.deepOrange))
                              .paddingLeft(4.0)
                        ],
                      ).paddingTop(8.0)
                    ],
                  ),
                  Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text("Prize Pool - 5000",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.deepOrange)),
                        SizedBox(height: 8),
                        Container(color: Colors.grey, height: 0.7, width: 70)
                            .elevation(0, elevation: 4),
                        SizedBox(height: 8),
                        Text("Entry Fee - 50",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.deepOrange)),
                        SizedBox(height: 8),
                        Container(color: Colors.grey, height: 0.7, width: 70)
                            .elevation(0, elevation: 4),
                        SizedBox(height: 8),
                        Text("Per Kill - 45",
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.bold,
                                color: Colors.deepOrange)),
                      ]).paddingTop(8.0)
                ],
              ).paddingAll(top: 16, left: 16, right: 16),*/
            ],
          ),
          Positioned(
              bottom: 50,
              right: 40,
              child: InkWell(
                onTap: (){
                  getJoinedMatchResponse(context,paidMatches);
                //  Navigator.pushNamed(context, BookSlotDetailScreen.routeName,arguments: paidMatches);
                },
                child: Container(
                  height: 34,
                  width: 120,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(24),
                      border: Border.all(color: Colors.deepOrange, width: 1)),
                  child: Center(child: Text("Join", style: TextStyle(color: Colors.deepOrange, fontWeight: FontWeight.w500, fontSize: 18),)),
                ),
              ))
        ],
      ),
    );
  }


  @override
  String screenName() => "PUBG MOBILE";
}

class WaveClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height * 0.80);
    path.quadraticBezierTo(size.width * 0.25, size.height * 0.90,
        size.width * 0.55, size.height * 0.80);
    path.cubicTo(size.width * 0.65, size.height * 0.77, size.width * 0.75,
        size.height * 0.73, size.width, size.height * 0.73);
    /*path.quadraticBezierTo(
        size.width * 0.95, size.height * 0.705, size.width, size.height * 0.70);*/
    path.lineTo(size.width, 0);
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}

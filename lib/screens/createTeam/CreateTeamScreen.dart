import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/model/Player.dart';
import 'package:ekojpubg/model/drawer/UserTeamsResponse.dart';
import 'package:ekojpubg/model/network/login_user.dart';
import 'package:ekojpubg/screens/createTeam/service/UserTeamService.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/widgets/CreateTeamItem.dart';
import 'package:ekojpubg/widgets/ProgressDialogType.dart';
import 'package:ekojpubg/widgets/TeamItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:ekojpubg/utils/extensions/context.dart';

class CreateTeamScreen extends BaseStatefulWidget {
  static const routeName = '/create_team';

  @override
  _CreateTeamScreenState createState() => _CreateTeamScreenState();
}

class _CreateTeamScreenState extends BaseState<CreateTeamScreen>
    with BaseStatefulScreen {
  _CreateTeamScreenState() : super(defaultPadding: false);
  ProgressDialog progressDialog;
  UserTeamService userTeamService;
  List<Data> userTeamList;
  UserTeamResponse userTeamResponse;
  List<Data> duoList=[];
  List<Data> squadList=[];
  List<Data> substituteList=[];
  Data duoTeamTypeUser=Data();
  Data squadTeamTypeUser=Data();
  LoginUserData userDetail;
  @override
  void initState() {
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    userTeamService = UserTeamService();

    getUserTeams();
    getDuoTeamTypeUser();
    getSquadTeamTypeUser();
    getUserDetail();
    super.initState();
  }

  @override
  Widget buildBody(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.all(16),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Text("Create A Team", style: TextStyle(color: Colors.red)),
          SizedBox(height: 24),
          Text("Duo", style: TextStyle(color: Colors.black, fontSize: 17)),
          SizedBox(height: 4),
         userTeamList!=null && userTeamList.length>0? CreateTeamItem("duo",userTeam: duoList,):SizedBox(),
          SizedBox(height: 16),
          Container(
                  color: Colors.black,
                  height: 0.7,
                  width: MediaQuery.of(context).size.width)
              .elevation(0),
          SizedBox(height: 24),
          Text("Squad", style: TextStyle(color: Colors.black, fontSize: 17)),
          SizedBox(height: 4),
          //TeamItem(""),
          SizedBox(height: 16),
          Container(
                  color: Colors.black,
                  height: 0.7,
                  width: MediaQuery.of(context).size.width)
              .elevation(0),
          SizedBox(height: 30),
          Text("Add Substitutes", style: TextStyle(color: Colors.black)),
          SizedBox(height: 4),
         /* TeamItem("", player: [
            PlayerModel(profilePic: "assets/images/ic_add.png"),
            PlayerModel(profilePic: "assets/images/ic_add.png")
          ], callback: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return SimpleDialog(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
                      title: Text("Player"),
                      children: <Widget>[
                        SizedBox(height: 16),
                        getTextField(context, TextEditingController(), "IGN").paddingAll(left: 8, right: 8),
                        SizedBox(height: 16),
                        getTextField(context, TextEditingController(), "ID").paddingAll(left: 8, right: 8),
                        SizedBox(height: 16),
                      ]..add(Container(
                          margin: EdgeInsets.only(top: 16),
                          child: Center(
                            child: SizedBox(
                              height: 42,
                              width: 100,
                              child: RaisedButton(
                                elevation: 5.0,
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                color: Colors.black,
                                shape: new RoundedRectangleBorder(
                                  borderRadius: new BorderRadius.circular(14.0),
                                ),
                                child: Text(
                                  'SAVE',
                                  style: TextStyle(
                                    fontWeight: FontWeight.w800,
                                    fontSize: 14.0,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        )));
                });
          }),*/
          Expanded(
            child: Align(
              alignment: Alignment.bottomRight,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Container(
                    height: 42,
                    width: 150,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(24),
                        color: Colors.black),
                    child: Center(
                        child: Text(
                      "Save Changes",
                      style: TextStyle(color: Colors.white),
                    )),
                  ).paddingAll(bottom: 60)
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget getTextField(context, controller, String hint, {bool pass = false}) =>
      SizedBox(
        height: 45,
        child: TextField(
          obscureText: pass,
          autofocus: false,
          controller: controller,
          cursorColor: Colors.white,
          textAlignVertical: TextAlignVertical.center,
          style: TextStyle(
              fontSize: 14.0, color: Colors.black87, letterSpacing: 1.2),
          decoration: InputDecoration(
            filled: true,
            floatingLabelBehavior: FloatingLabelBehavior.never,
            fillColor: Color(0xFFEAEAEA),
            hintText: hint,
            hintStyle: TextStyle(color: Colors.black87),
            contentPadding: EdgeInsets.only(left: 14.0, right: 14),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(8.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(8.0),
            ),
          ),
        ),
      );

  @override
  String screenName() {
    return "Team";
  }

  void getUserTeams()async{

    await progressDialog.show();
    userTeamService.getUserTeam("8","1").then((value) {
      if (value != null &&
          value.status == "success") {
        setState(() {
          userTeamResponse=value;
          userTeamList=value.data;
          if(userTeamList.length>0){
            userTeamList.forEach((element) {
              if(element.teamType=="duo"){
                if(element.id==element.captainId){
                  duoList.insert(0, element);
                }else{
                  duoList.add(element);
                }

              }else if(element.teamType=="squad"){
                if(element.id==element.captainId){
                  squadList.insert(0, element);
                }else{
                  squadList.add(element);
                }

              }else if(element.teamType=="substitute"){
                substituteList.add(element);
              }
            });
          }
        });

      }else if(value != null &&
          value.status != "success"){
        context.showAlert("Something went Wrong.");
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  Future<LoginUserData> getUserDetail() async{
    userDetail = await SharedPreferenceUtil.getUserDetail();
    return userDetail;
  }

   getDuoTeamTypeUser(){
    duoTeamTypeUser.name= "Simran";
   duoTeamTypeUser.teamType="duo";
   duoList.add(duoTeamTypeUser);

  }

  getSquadTeamTypeUser(){
    squadTeamTypeUser.name= "Simran";
    squadTeamTypeUser.teamType="squad";
    squadList.add(squadTeamTypeUser);
  }
}

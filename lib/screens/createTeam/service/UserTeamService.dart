
import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/drawer/UserTeamsResponse.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class UserTeamService {
  Future<UserTeamResponse> getUserTeam(userId,gameId) async{
    ApiResponse apiResponse= await ApiHitter.getApiResponse(AppConstants.USERTEAMS,
        queryParameters: {
          "user_id":userId,
          "game_id":gameId,

        });
    try{
      return apiResponse.response != null ?
      UserTeamResponse.fromJson(apiResponse.response.data)
          : (CommonResponse()..Message = apiResponse.msg) ;
    }catch(e){
      print(e..toString());
      return null;
    }
  }
}
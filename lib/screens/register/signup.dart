import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/screens/register/service/RegisterService.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/utils/extensions/context.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';
import 'package:ekojpubg/widgets/ProgressDialogType.dart';
import 'package:ekojpubg/widgets/TextFieldEye.dart';
import 'package:ekojpubg/widgets/roundcheckbox.dart';
import 'package:ekojpubg/widgets/validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';

class SignUpView extends BaseStatefulWidget {
  static const routeName = '/signup_view';

  @override
  _SignUpViewState createState() => _SignUpViewState();
}

class _SignUpViewState extends BaseState<SignUpView> with BaseStatefulScreen {
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final firstNameController = TextEditingController();
  final middleNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final confirmPassController = TextEditingController();
  final phoneController = TextEditingController();
  bool termsSelected = false;
  final RegisterService registerService = RegisterService();
  ProgressDialog progressDialog;

  _SignUpViewState() : super(defaultPadding: false);

  @override
  String screenName() => null;


  @override
  Widget buildBody(BuildContext context) {
    progressDialog = ProgressDialog(context);
    return SingleChildScrollView(
      child: Container(
        child: Stack(
          children: <Widget>[
            /* Positioned(
            top: 0,
            right: 0,
            left: 0,
            child: Container(
              height: 250.h,
              decoration: BoxDecoration(
              color: Colors.amberAccent
              ),
            ).clip(TopCurvedClipper()),
          ),*/
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                getLoginTopView().paddingTop(110.h),
                getTextField(context, firstNameController, "First Name")
                    .paddingE(
                        EdgeInsets.only(left: 24.w, right: 24.w, top: 80.h)),
                getTextField(
                        context, middleNameController, "Middle Name/Alternate")
                    .paddingE(
                        EdgeInsets.only(left: 24.w, right: 24.w, top: 24.h)),
                getTextField(context, lastNameController, "Last Name").paddingE(
                    EdgeInsets.only(left: 24.w, right: 24.w, top: 24.h)),
                PasswordField(passController, "Password").paddingE(
                    EdgeInsets.only(left: 24.w, right: 24.w, top: 24.h)),
                PasswordField(confirmPassController, "Confirm Password")
                    .paddingE(
                        EdgeInsets.only(left: 24.w, right: 24.w, top: 24.h)),
                getTextField(context, emailController, "Email").paddingE(
                    EdgeInsets.only(left: 24.w, right: 24.w, top: 24.h)),
                getTextField(context, phoneController, "Phone Number", isNumber: true).paddingE(
                    EdgeInsets.only(left: 24.w, right: 24.w, top: 24.h)),
                _showCheckbox().paddingTop(14.h),
                signInButton()
                    .paddingE(EdgeInsets.only(top: 12.h, bottom: 24.h))
              ],
            ),
          ],
        ),
      ),
    ).noScrollEffect();
  }

  Widget getLoginTopView() {
    return Column(
      children: <Widget>[
        Image.asset(
          "assets/images/app_icon.png",
          width: 50.w,
          height: 50.h,
          color: Colors.deepOrangeAccent,
          fit: BoxFit.fill,
        ),
        Text(
          "EKOJ",
          style: TextStyle(
              color: Colors.black,
              fontSize: 24.sp,
              fontWeight: FontWeight.bold),
        ).paddingTop(4.0.h),
        Text(
          "An eSport Platform",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.sp,
            //fontWeight: FontWeight.bold
          ),
        ).paddingTop(1.0.h),
      ],
    );
  }

  Widget getTextField(context, controller, String hint, {bool pass = false, bool isNumber = false}) =>
      SizedBox(
        height: 55.h,
        child: TextField(
          obscureText: pass,
          autofocus: false,
          controller: controller,
          keyboardType: isNumber ? TextInputType.number :  TextInputType.text,
          cursorColor: Colors.white,
          textAlignVertical: TextAlignVertical.center,
          style: TextStyle(
              fontSize: 22.0.sp, color: Colors.black87, letterSpacing: 1.2),
          decoration: InputDecoration(
            filled: true,
            floatingLabelBehavior: FloatingLabelBehavior.never,
            fillColor: Color(0xFFEAEAEA),
            hintText: hint,
            hintStyle: TextStyle(color: Colors.black87),
            contentPadding: EdgeInsets.only(left: 14.0.w, right: 14.w),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(8.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(8.0),
            ),
          ),
        ),
      );

  Widget _showCheckbox() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        RoundCheckbox(
            tristate: false,
            inactiveColor: Colors.black,
            materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
            value: termsSelected,
            activeColor: Colors.black,
            onChanged: (bool value) {
              setState(() {
                termsSelected = value;
              });
            }).paddingTop(2.0.h),
        Container(
          child: RichText(
            text: new TextSpan(
              // Note: Styles for TextSpans must be explicitly defined.
              // Child text spans will inherit styles from parent
              style: new TextStyle(
                  fontSize: 22.0.sp,
                  color: Colors.black,
                  fontWeight: FontWeight.bold),
              children: <TextSpan>[
                new TextSpan(text: 'Agree '),
                new TextSpan(
                    text: 'Terms & Conditions \n Privacy Policy',
                    recognizer: new TapGestureRecognizer()
                      ..onTap = () => {
                            // Navigator.of(context).pushReplacementNamed(SignUpView.routeName)
                          },
                    style: TextStyle(color: Colors.lightBlue)),
              ],
            ),
          ),
        ).paddingLeft(12.0.w)
      ],
    );
  }

  Widget signInButton() => SizedBox(
        width: 180.w,
        height: 55.h,
        child: RaisedButton(
          //splashColor: Colors.lightBlue,
          onPressed: () {
            if (validate()) {
              registerUser();
            }
          },
          color: Color(0xFF9BADD7),
          child: Text("Register",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 22.sp,
                  fontWeight: FontWeight.bold)),
          //disabledColor: Colors.lightBlue,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        ),
      ).elevation(24.0, elevation: 8.0);

  void registerUser() async {
    await progressDialog.show();
    final email = emailController.text;
    final pass = passController.text;
    final firstName = firstNameController.text;
    final middleName = middleNameController.text;
    final lastName = lastNameController.text;
    final phone = phoneController.text;
    registerService
        .registerUser(firstName, lastName, middleName, email, pass, phone)
        .then((value) async {
      if (value != null && value.status == "success") {
       await SharedPreferenceUtil.saveUserId(value.data.userId.toString());
        Navigator.pop(context);
      } else {
        context.showAlert(value?.message ?? "Sorry, Something went wrong");
      }
    }).catchError((e) {
      context.showAlert(e?.message ?? "Sorry, Something went wrong");
    }).whenComplete(()async { await progressDialog.hide();});
  }

  bool validate() {
    final email = emailController.text;
    final pass = passController.text;
    final firstName = firstNameController.text;
    //final middleName = middleNameController.text;
    //final lastName = lastNameController.text;
    final confirmPass = confirmPassController.text;
    final phone = phoneController.text;

    if (firstName.isEmpty) {
      context.showAlert("First name can't be empty.");
      return false;
    }
    if (!Validator.isPassword(pass, minLength: 8)) {
      context.showAlert("Password must be of minimum of 8 character.");
      return false;
    }
    if (pass != confirmPass) {
      context.showAlert("Password and Confirm pass does not match.");
      return false;
    }
    if (!Validator.isEmail(email)) {
      context.showAlert("Please enter valid Email.");
      return false;
    }

    if (phone.length < 10 || phone.length > 10) {
      context.showAlert("Please enter valid Phone number.");
      return false;
    }
    if (!termsSelected) {
      context.showAlert("Please accept Terms & Conditions.");
      return false;
    }

    return true;
  }
}

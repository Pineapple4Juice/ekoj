import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/network/login_user.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class RegisterService {
  Future<LoginUser> registerUser(
      firstName, lastName, middleName, email, password, phoneNumber,
      {loginType = "native", socialToken}) async {
    ApiResponse apiResponse =
        await ApiHitter.getPostApiResponse(AppConstants.REGISTER, data: {
      "first_name": firstName,
      "last_name": lastName,
      "middle_name": middleName,
      "email": email,
      "password": password,
      "confirm_password": password,
      "phone_number": phoneNumber,
      "login_type": loginType,
      socialToken == null ? ("nothing") : ("social_token"): socialToken ?? ""
    });

    try {
      return apiResponse.response != null
          ? LoginUser.fromJson(apiResponse.response.data)
          : (CommonResponse()..Message = apiResponse.msg);
    } catch (e) {
      print(e..toString());
      return null;
    }
  }
}

import 'package:ekojpubg/app/app_theme.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/model/drawer/HireResultRespinse.dart';
import 'package:ekojpubg/screens/hire_result/HireScreen.dart';
import 'package:ekojpubg/screens/hire_result/service/HireResultService.dart';
import 'package:ekojpubg/widgets/ProgressDialogType.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'ResultScreen.dart';

class HireAndResultScreen extends StatefulWidget {
  static const routeName = '/hire_result';
  @override
  _HireAndResultScreenState createState() => _HireAndResultScreenState();
}

class _HireAndResultScreenState extends State<HireAndResultScreen> {

  ProgressDialog progressDialog;
  HireResultService hireResultService;
  List<Data> hireResultList;
  List<Data> hireList;
  List<Data> resultList;

  @override
  void initState() {
    super.initState();
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    hireResultService = HireResultService();
    getHireResultData();
  }

  void getHireResultData() async {
    final userId = await SharedPreferenceUtil.getUserid();
    await progressDialog.show();
    hireResultService.getHireResultData("8").then((value) {
      if (value != null &&
          value.status == "success" &&
          value.data != null &&
          value.data.isNotEmpty) {
        setState(() {
          hireResultList=value.data;
          hireList=value.data.where((element) => element.category=="Hire").toList();
          resultList=value.data.where((element) => element.category=="Result").toList();
        });
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
          appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Icon(
            Icons.keyboard_arrow_left,
            color: Colors.black,
            size: 32,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Colors.white,
        title: Text("Hire & Results", style: AppTheme.titleBlack),
        bottom: TabBar(
          labelStyle: TextStyle(color: Colors.blue, fontSize: 16, fontWeight: FontWeight.w500),
          unselectedLabelStyle: TextStyle(color: Colors.black, fontSize: 16, fontWeight: FontWeight.w500),
            labelColor: Colors.blue,
            unselectedLabelColor: Colors.black,
            indicatorColor: Colors.blue,
            tabs: [
              Tab(
                text: "Hire",
              ),
              Tab(
                text: "Results",
              )
            ]),
      ),
      body: TabBarView(children: [
        hireList!=null && hireList.length>0? HireScreen(hireList) : Center(child: Text("No Result Found",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),),
        resultList!=null && resultList.length>0?ResultScreen(resultList): Center(child: Text("No Result Found",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),),
      ]),
      ),
    );
  }
}



import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/drawer/HireResultRespinse.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class HireResultService{
  Future<HireResultResponse> getHireResultData(userId) async {

    //?user_id=8&game_id=1
    ApiResponse apiResponse =
    await ApiHitter.getApiResponse(AppConstants.HIRERESULT, queryParameters: {
      "user_id" : userId
    });

    try {
      return apiResponse.response != null
          ? HireResultResponse.fromJson(apiResponse.response.data)
          : (CommonResponse()
        ..Message = apiResponse.msg);
    } catch (e) {
      print(e..toString());
      return null;
    }
  }
}
import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/drawer/HireResultRespinse.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HireScreen extends StatelessWidget {
   List<Data> hireList;
  HireScreen(this.hireList);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      shrinkWrap: true,
        itemCount: this.hireList.length,
        itemBuilder: (context, pos) {
          return HireItem(this.hireList[pos]);
        });
  }
}

class HireItem extends StatelessWidget {
  Data hireItem;
  HireItem(this.hireItem);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.black54,
      height: MediaQuery.of(context).size.height * 0.16,
      width: MediaQuery.of(context).size.width,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width*0.97 ,
            height: MediaQuery.of(context).size.height * 0.13,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: NetworkImage(AppConstants.domain +
                  hireItem.image),
                fit: BoxFit.fill
              )
            ),
          )
         /* Image.asset(
            "assets/images/pubg_cover.jpg",
            height: MediaQuery.of(context).size.height * 0.13,
            width: MediaQuery.of(context).size.height * 0.12,
            fit: BoxFit.cover,
          ).paddingLeft(16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "Name: XYZ",
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
                Text(
                  "GAME: PUBG",
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ).paddingTop(8),
                Text(
                  "ACHIEVEMENTS: Crew Challenge, PMCO Finalist",
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ).paddingTop(8),
              ],
            ).paddingLeft(16),
          )*/
        ],
      ),
    ).elevation(0).paddingTop(16);
  }
}

import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/Result.dart';
import 'package:ekojpubg/model/drawer/HireResultRespinse.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:flutter/material.dart';

class ResultScreen extends StatefulWidget {
  List<Data> resultData;
  ResultScreen(this.resultData);
  @override
  _ResultScreenState createState() => _ResultScreenState();
}

class _ResultScreenState extends State<ResultScreen> {
  final resultList = [
    [
      ResultModel("Team Secret", "22", "45", "39", "12", "120"),
      ResultModel("Team Bliss", "22", "45", "39", "12", "120"),
      ResultModel("Team Jadu", "22", "45", "39", "12", "120"),
      ResultModel("Team Entity", "22", "45", "39", "12", "120"),
      ResultModel("Team TSM", "22", "45", "39", "12", "120"),
      ResultModel("Team MegaStar", "22", "45", "39", "12", "120"),
      ResultModel("Team Ranaldo", "22", "45", "39", "12", "120"),
      ResultModel("Team Empire", "22", "45", "39", "12", "120"),
      ResultModel("Team uHigh", "22", "45", "39", "12", "120")
    ],
    [
      ResultModel("Team Secret", "22", "45", "39", "12", "120"),
      ResultModel("Team Bliss", "22", "45", "39", "12", "120"),
      ResultModel("Team Jadu", "22", "45", "39", "12", "120"),
      ResultModel("Team Entity", "22", "45", "39", "12", "120"),
      ResultModel("Team TSM", "22", "45", "39", "12", "120"),
      ResultModel("Team MegaStar", "22", "45", "39", "12", "120"),
      ResultModel("Team Ranaldo", "22", "45", "39", "12", "120"),
      ResultModel("Team Empire", "22", "45", "39", "12", "120"),
      ResultModel("Team uHigh", "22", "45", "39", "12", "120")
    ],
    [
      ResultModel("Team Secret", "22", "45", "39", "12", "120"),
      ResultModel("Team Bliss", "22", "45", "39", "12", "120"),
      ResultModel("Team Jadu", "22", "45", "39", "12", "120"),
      ResultModel("Team Entity", "22", "45", "39", "12", "120"),
      ResultModel("Team TSM", "22", "45", "39", "12", "120"),
      ResultModel("Team MegaStar", "22", "45", "39", "12", "120"),
      ResultModel("Team Ranaldo", "22", "45", "39", "12", "120"),
      ResultModel("Team Empire", "22", "45", "39", "12", "120"),
      ResultModel("Team uHigh", "22", "45", "39", "12", "120")
    ],
    [
      ResultModel("Team Secret", "22", "45", "39", "12", "120"),
      ResultModel("Team Bliss", "22", "45", "39", "12", "120"),
      ResultModel("Team Jadu", "22", "45", "39", "12", "120"),
      ResultModel("Team Entity", "22", "45", "39", "12", "120"),
      ResultModel("Team TSM", "22", "45", "39", "12", "120"),
      ResultModel("Team MegaStar", "22", "45", "39", "12", "120"),
      ResultModel("Team Ranaldo", "22", "45", "39", "12", "120"),
      ResultModel("Team Empire", "22", "45", "39", "12", "120"),
      ResultModel("Team uHigh", "22", "45", "39", "12", "120")
    ],
    [
      ResultModel("Team Secret", "22", "45", "39", "12", "120"),
      ResultModel("Team Bliss", "22", "45", "39", "12", "120"),
      ResultModel("Team Jadu", "22", "45", "39", "12", "120"),
      ResultModel("Team Entity", "22", "45", "39", "12", "120"),
      ResultModel("Team TSM", "22", "45", "39", "12", "120"),
      ResultModel("Team MegaStar", "22", "45", "39", "12", "120"),
      ResultModel("Team Ranaldo", "22", "45", "39", "12", "120"),
      ResultModel("Team Empire", "22", "45", "39", "12", "120"),
      ResultModel("Team uHigh", "22", "45", "39", "12", "120")
    ],
  ];

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: widget.resultData.length,
        itemBuilder: (context, pos) {
          return ResultImageItem(widget.resultData[pos]);
        });
  }

  Widget ResultImageItem(Data resultItem){
    return Container(
      color: Colors.black54,
      height: MediaQuery.of(context).size.height * 0.44,
      width: MediaQuery.of(context).size.width,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width*0.97 ,
            height: MediaQuery.of(context).size.height * 0.40,
            decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(AppConstants.domain +
                        resultItem.image),
                    fit: BoxFit.fill
                )
            ),
          )
          /* Image.asset(
            "assets/images/pubg_cover.jpg",
            height: MediaQuery.of(context).size.height * 0.13,
            width: MediaQuery.of(context).size.height * 0.12,
            fit: BoxFit.cover,
          ).paddingLeft(16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  "Name: XYZ",
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ),
                Text(
                  "GAME: PUBG",
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ).paddingTop(8),
                Text(
                  "ACHIEVEMENTS: Crew Challenge, PMCO Finalist",
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(color: Colors.white, fontSize: 14),
                ).paddingTop(8),
              ],
            ).paddingLeft(16),
          )*/
        ],
      ),
    ).elevation(0).paddingTop(16);
  }

}

class ResultItem extends StatelessWidget {
  final List<ResultModel> resultList;

  ResultItem(this.resultList);

  @override
  Widget build(BuildContext context) {
    final Size size = MediaQuery.of(context).size;
    return Table(
      border: TableBorder.all(color: Colors.white),
      columnWidths: {
        0: FixedColumnWidth(size.width * 0.10),
        1: FixedColumnWidth(size.width * 0.28),
        2: FixedColumnWidth(size.width * 0.10),
        3: FixedColumnWidth(size.width * 0.10),
        4: FixedColumnWidth(size.width * 0.10),
        5: FixedColumnWidth(size.width * 0.10),
        6: FixedColumnWidth(size.width * 0.22),
      },
      children: [
        TableRow(decoration: BoxDecoration(color: Colors.black), children: [
          Center(child: Text("S\nno.", style: TextStyle(color: Colors.white))),
          Center(
              child: Text("Team name", style: TextStyle(color: Colors.white))
                  .paddingTop(4)),
          Center(
              child: Text("E.", style: TextStyle(color: Colors.white))
                  .paddingTop(4)),
          Center(
              child: Text("M.", style: TextStyle(color: Colors.white))
                  .paddingTop(4)),
          Center(
              child: Text("V.", style: TextStyle(color: Colors.white))
                  .paddingTop(4)),
          Center(
              child: Text("S.", style: TextStyle(color: Colors.white))
                  .paddingTop(4)),
          Center(
              child: Text("TOTAL", style: TextStyle(color: Colors.white))
                  .paddingTop(4)),
        ])
      ]..addAll(resultList
          .asMap()
          .entries
          .map((result) => TableRow(
                  decoration: BoxDecoration(
                      color: result.key % 3 == 0
                          ? Colors.grey.withAlpha(250)
                          : Colors.grey.withAlpha(130)),
                  children: [
                    Center(
                        child: Text("${result.key}.",
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white))
                            .paddingAll(top: 4, bottom: 4)),
                    Center(
                        child: Text(result.value.teamName,
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white))
                            .paddingAll(top: 4, bottom: 4)),
                    Center(
                        child: Text(result.value.e,
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white))
                            .paddingAll(top: 4, bottom: 4)),
                    Center(
                        child: Text(result.value.m,
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white))
                            .paddingAll(top: 4, bottom: 4)),
                    Center(
                        child: Text(result.value.v,
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white))
                            .paddingAll(top: 4, bottom: 4)),
                    Center(
                        child: Text(result.value.s,
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white))
                            .paddingAll(top: 4, bottom: 4)),
                    Center(
                        child: Text(result.value.total,
                                textAlign: TextAlign.center,
                                style: TextStyle(color: Colors.white))
                            .paddingAll(top: 4, bottom: 4)),
                  ]))
          .toList()),
    ).elevation(0).paddingTop(16);
  }
}

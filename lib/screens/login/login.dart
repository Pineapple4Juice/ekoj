import 'package:ekojpubg/auth/sign_in.dart';
import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/screens/home/home_screen.dart';
import 'package:ekojpubg/screens/login/service/LoginService.dart';
import 'package:ekojpubg/screens/register/service/RegisterService.dart';
import 'package:ekojpubg/screens/register/signup.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/utils/extensions/context.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';
import 'package:ekojpubg/widgets/ProgressDialogType.dart';
import 'package:ekojpubg/widgets/TextFieldEye.dart';
import 'package:ekojpubg/widgets/validator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_login_facebook/flutter_login_facebook.dart';

class LoginView extends BaseStatefulWidget {
  static const routeName = '/login_view';

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends BaseState<LoginView> with BaseStatefulScreen {
  final emailController = TextEditingController();
  final passController = TextEditingController();
  final LoginService loginService = LoginService();
  final RegisterService registerService = RegisterService();
  ProgressDialog progressDialog;
  final fb = FacebookLogin();

  _LoginViewState() : super(defaultPadding: false);

  @override
  String screenName() => null;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget buildBody(BuildContext context) {
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    return SingleChildScrollView(
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          children: <Widget>[
            /*Positioned(
              top: 0,
              right: 0,
              left: 0,
              child: Container(
                height: 250.h,
                decoration: BoxDecoration(
                    color: Colors.amberAccent
                ),
              ).clip(TopCurvedClipper()),
            ),*/
            Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                getLoginTopView().paddingTop(110.h),
                getTextField(
                  context,
                  emailController,
                  "Email",
                ).paddingE(EdgeInsets.only(left: 24.w, right: 24.w, top: 80.h)),
                SizedBox(
                  height: 24.h,
                ),
                PasswordField(passController, "Password")
                    .paddingE(EdgeInsets.only(left: 24.w, right: 24.w)),
                SizedBox(
                  height: 41.h,
                ),
                loginButton(),
                textUnderButton(),
                needHelp(),
                Expanded(
                    child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    children: <Widget>[
                      signInWith(),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          InkWell(
                            onTap: () {
                              facebookLogin(fb);
                            },
                            child: Image.asset(
                              "assets/images/facebook_circled.png",
                              width: 50.w,
                              height: 50.h,
                            ),
                          ),
                          SizedBox(
                            width: 12.w,
                          ),
                          InkWell(
                            onTap: () async {
                              await progressDialog.show();
                              signInWithGoogle().whenComplete(() {
                                afterGoogleNavigation();
                                // Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
                              });
                            },
                            child: Container(
                              width: 50.w,
                              height: 50.h,
                              padding: EdgeInsets.all(4.0),
                              decoration: BoxDecoration(
                                  color: Colors.white, shape: BoxShape.circle),
                              child: Image.asset(
                                "assets/images/google_logo.png",
                                width: 30.w,
                                height: 30.h,
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                ))
              ],
            ),
          ],
        ),
      ),
    ).noScrollEffect();
  }

  Future afterGoogleNavigation() async {
    final FirebaseUser currentUser = await auth.currentUser();
    registerService
        .registerUser(
            currentUser.displayName, "", "", currentUser.email ?? "", "", "",
            loginType: "social", socialToken: currentUser.uid)
        .then((value) async {
      progressDialog.hide();
      if (value != null && value.status == "success") {
        if (value.data != null) {
          await SharedPreferenceUtil.saveUser(value.data);
          Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
        } else
          Navigator.pop(context);
      } else {
        context.showAlert(value?.message ?? "Sorry, Something went wrong");
      }
    }).catchError((e) {
      progressDialog.hide();
      context.showAlert(e?.message ?? "Sorry, Something went wrong");
    });
  }

  Future facebookLogin(FacebookLogin fb) async {
    await progressDialog.show();
    try {
      final res = await fb.logIn(permissions: [
        FacebookPermission.publicProfile,
        FacebookPermission.email,
      ]);

      // Check result status
      switch (res.status) {
        case FacebookLoginStatus.Success:
          // Logged in
          // Send access token to server for validation and auth
          final FacebookAccessToken accessToken = res.accessToken;
          print('Access token: ${accessToken.token}');

          // Get profile data
          final profile = await fb.getUserProfile();
          print('Hello, ${profile.name}! You ID: ${profile.userId}');

          // Get user profile image url
          final imageUrl = await fb.getProfileImageUrl(width: 100);
          print('Your profile image: $imageUrl');

          // Get email (since we request email permission)
          final email = await fb.getUserEmail();

          // But user can decline permission
          if (email != null) print('And your email is $email');

          registerService
              .registerUser(profile.name, "", "", email ?? "", "", "",
                  loginType: "social", socialToken: profile.userId)
              .then((value) async {
            progressDialog.hide();
            if (value != null && value.status == "success") {
              if (value.data != null && value.data.isApproved) {
                await SharedPreferenceUtil.saveUserId(
                    value.data.userId.toString());

                Navigator.pushReplacementNamed(context, HomeScreen.routeName);
              } else
                Navigator.pop(context);
            } else {
              context
                  .showAlert(value?.message ?? "Sorry, Something went wrong");
            }
          }).catchError((e) {
            progressDialog.hide();
            context.showAlert(e?.message ?? "Sorry, Something went wrong");
          });

          break;
        case FacebookLoginStatus.Cancel:
          // User cancel log in
          await progressDialog.hide();
          break;
        case FacebookLoginStatus.Error:
          // Log in failed
          print('Error while log in: ${res.error}');
          await progressDialog.hide();
          break;
      }
    } catch (e) {
      print(e);
      await progressDialog.hide();
    }
  }

  Widget getLoginTopView() {
    return Column(
      children: <Widget>[
        Image.asset(
          "assets/images/app_icon.png",
          width: 50.w,
          height: 50.h,
          color: Colors.deepOrangeAccent,
          fit: BoxFit.fill,
        ),
        Text(
          "EKOJ",
          style: TextStyle(
              color: Colors.black,
              fontSize: 24.sp,
              fontWeight: FontWeight.bold),
        ).paddingTop(4.0.h),
        Text(
          "An eSport Platform",
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.sp,
            //fontWeight: FontWeight.bold
          ),
        ).paddingTop(1.0.h),
      ],
    );
  }

  Widget getTextField(context, controller, String hint, {bool pass = false}) =>
      SizedBox(
        height: 55.h,
        child: TextField(
          obscureText: pass,
          autofocus: false,
          controller: controller,
          cursorColor: Colors.white,
          textAlignVertical: TextAlignVertical.center,
          style: TextStyle(
              fontSize: 22.0.sp, color: Colors.black87, letterSpacing: 1.2),
          decoration: InputDecoration(
            filled: true,
            floatingLabelBehavior: FloatingLabelBehavior.never,
            fillColor: Color(0xFFEAEAEA),
            hintText: hint,
            hintStyle: TextStyle(color: Colors.black87),
            contentPadding: EdgeInsets.only(left: 14.0.w, right: 14.w),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(8.0),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: Colors.white),
              borderRadius: BorderRadius.circular(8.0),
            ),
          ),
        ),
      );

  Widget loginButton() => SizedBox(
        width: 180.w,
        height: 55.h,
        child: RaisedButton(
          //splashColor: Colors.lightBlue,
          onPressed: () {
            //Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
            if (validate()) {
              normalLogin();
            }
          },
          color: Color(0xFF9BADD7),
          child: Text("Login",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 22.sp,
                  fontWeight: FontWeight.bold)),
          //disabledColor: Colors.lightBlue,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(24)),
        ),
      ).elevation(24.0, elevation: 8.0);

  void normalLogin() async {
    await progressDialog.show();
    loginService
        .loginUser(emailController.text, passController.text)
        .then((value) async {
      progressDialog.hide();
      if (value != null && value.status == "success") {
        await SharedPreferenceUtil.saveUser(value.data);
        await SharedPreferenceUtil.saveUserId(value.data.userId.toString());
        Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
      } else {
        context.showAlert(value?.message ?? "Sorry, Something went wrong");
      }
    }).catchError((e) {
      progressDialog.hide();
      context.showAlert(e?.message ?? "Sorry, Something went wrong");
    });
  }

  Widget textUnderButton() => Container(
        child: RichText(
          text: new TextSpan(
            // Note: Styles for TextSpans must be explicitly defined.
            // Child text spans will inherit styles from parent
            style: new TextStyle(
                fontSize: 22.0.sp,
                color: Colors.black,
                fontWeight: FontWeight.bold),
            children: <TextSpan>[
              new TextSpan(text: 'Not A Member? '),
              new TextSpan(
                  text: 'Register ',
                  recognizer: new TapGestureRecognizer()
                    ..onTap = () =>
                        {Navigator.of(context).pushNamed(SignUpView.routeName)},
                  style: TextStyle(color: Colors.lightBlue)),
              new TextSpan(text: 'Here'),
            ],
          ),
        ),
      ).paddingTop(12.0.h);

  Widget needHelp() => Text(
        "Need Help?",
        style: TextStyle(color: Colors.black, fontSize: 18.0.sp),
      ).paddingTop(6.0.h);

  Widget signInWith() => Text(
        "Sign In With",
        style: TextStyle(color: Colors.black, fontSize: 18.0.sp),
      ).paddingTop(48.h);

  bool validate() {
    if (!Validator.isEmail(emailController.text)) {
      context.showAlert("Please enter valid Email.");
      return false;
    }
    if (!Validator.isPassword(passController.text, minLength: 8)) {
      context.showAlert("Please enter valid password");
      return false;
    }
    return true;
  }
}

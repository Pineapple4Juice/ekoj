import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/network/login_user.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class LoginService {

  Future<LoginUser> loginUser(email, password) async {
    ApiResponse apiResponse = await ApiHitter.getPostApiResponse(
        AppConstants.LOGIN, data: {
      "email":email,
      "password":password
    });

    try {
      return apiResponse.response != null ? LoginUser.fromJson(
          apiResponse.response.data) : (LoginUser()
        ..Message = apiResponse.msg);
    } catch (e) {
      print(e..toString());
      return null;
    }
  }

}
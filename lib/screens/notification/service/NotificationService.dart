
import 'package:ekojpubg/commons/constants/app_constants.dart';
import 'package:ekojpubg/model/home/NotificationResponse.dart';
import 'package:ekojpubg/model/network/common_response.dart';
import 'package:ekojpubg/network/dio_rest_client.dart';

class NotificationService{

  Future<NotificationResponse> getAllNotifications(userId) async{
    ApiResponse apiResponse= await ApiHitter.getApiResponse(AppConstants.NOTIFICATIONS,
    queryParameters: {
      "user_id":userId
    });
    try{
      return apiResponse.response != null ?
      NotificationResponse.fromJson(apiResponse.response.data)
          : (CommonResponse()..Message = apiResponse.msg) ;
    }catch(e){
      print(e..toString());
      return null;
    }
  }

}
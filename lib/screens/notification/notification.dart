import 'package:ekojpubg/base/base_state.dart';
import 'package:ekojpubg/base/base_stateful_widget.dart';
import 'package:ekojpubg/base/base_statefull_screen.dart';
import 'package:ekojpubg/commons/shared_preference.dart';
import 'package:ekojpubg/model/home/NotificationResponse.dart';
import 'package:ekojpubg/screens/notification/service/NotificationService.dart';
import 'package:ekojpubg/utils/responsive/flutter_screenutil.dart';
import 'package:ekojpubg/widgets/ProgressDialogType.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:ekojpubg/utils/extensions/context.dart';

class NotificationScreen extends BaseStatefulWidget{
  static const routeName = '/notification';
  @override
  _NotificationScreenState createState() =>  _NotificationScreenState();

}

class _NotificationScreenState extends BaseState<NotificationScreen> with BaseStatefulScreen {

  ProgressDialog progressDialog;
  NotificationService notificationService;
  List<Data> notificationList;

  @override
  String screenName(){
    return "Notification";}

    @override
  void initState() {
    super.initState();
    progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: false, showLogs: true);
    notificationService = NotificationService();
    getAllNotifications();
  }

  void getAllNotifications()async{
    final userId = await SharedPreferenceUtil.getUserid();
    await progressDialog.show();
    notificationService.getAllNotifications("8").then((value) {
      if (value != null &&
          value.status == "success") {
        setState(() {
          notificationList=value.data;
        });

      }else if(value != null &&
          value.status != "success"){
        context.showAlert("Something went Wrong.");
      }
    }).whenComplete(() async {
      await progressDialog.hide();
    });
  }

  @override
  Widget buildBody(BuildContext context) {
    ScreenUtil.init(context);
    return
      notificationList!=null && notificationList.length >0 ?Container(
      height: MediaQuery.of(context).size.height,
     width: MediaQuery.of(context).size.width,
     child: ListView.builder(
       itemCount: notificationList.length,
       itemBuilder:(context,position){
        return itemView(notificationList[position]);
       }),
    ):Center(
        child: Text(" No Notifications Found."),
      );
  }

  Widget itemView(Data itemData){
    return Container(

      padding: EdgeInsets.all(16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width * 0.70,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              Text(itemData.title,style: TextStyle(
                fontSize: 26.0.sp,
              ),),
              Text(itemData.description,
                textAlign: TextAlign.left,
                style: TextStyle(
                fontSize: 18.0.sp,
              ),).paddingTop(4.0.h)
            ],
            ),
          ),
          Container(
            width: 60.w,
            height: 60.h,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: Color(0xFFE6E6E6),),
            child: Icon(Icons.notifications,
            color: Color(0xFFB8B8B8),
            size: 30,),
          )

        ],
      ),
    );
  }



}


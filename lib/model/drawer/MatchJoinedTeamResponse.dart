class MatchJoinedTeamResponse {
  String status;
  List<Team> data;

  MatchJoinedTeamResponse({this.status, this.data});

  MatchJoinedTeamResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Team>();
      json['data'].forEach((v) {
        data.add(new Team.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Team {
  String name;
  int count;
  int captainId;
  String teamType;
  List<MemberIds> memberIds;

  Team({this.name, this.count, this.captainId, this.teamType, this.memberIds});

  Team.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    count = json['count'];
    captainId = json['captain_id'];
    teamType = json['team_type'];
    if (json['member_ids'] != null) {
      memberIds = new List<MemberIds>();
      json['member_ids'].forEach((v) {
        memberIds.add(new MemberIds.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['count'] = this.count;
    data['captain_id'] = this.captainId;
    data['team_type'] = this.teamType;
    if (this.memberIds != null) {
      data['member_ids'] = this.memberIds.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MemberIds {
  int user;
  String firstName;
  String middleName;
  String lastName;
  String email;
  String phoneNumber;
  String profilePic;
  bool isActive;
  String memberType;

  MemberIds(
      {this.user,
        this.firstName,
        this.middleName,
        this.lastName,
        this.email,
        this.phoneNumber,
        this.profilePic,
        this.isActive,
        this.memberType});

  MemberIds.fromJson(Map<String, dynamic> json) {
    user = json['user'];
    firstName = json['first_name'];
    middleName = json['middle_name'];
    lastName = json['last_name'];
    email = json['email'];
    phoneNumber = json['phone_number'];
    profilePic = json['profile_pic'];
    isActive = json['is_active'];
    memberType = json['member_type'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user'] = this.user;
    data['first_name'] = this.firstName;
    data['middle_name'] = this.middleName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['phone_number'] = this.phoneNumber;
    data['profile_pic'] = this.profilePic;
    data['is_active'] = this.isActive;
    data['member_type'] = this.memberType;
    return data;
  }
}

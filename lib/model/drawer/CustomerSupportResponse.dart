class CustomSupportResponse {
  String status;
  List<Data> data;

  CustomSupportResponse({this.status, this.data});

  CustomSupportResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String socialMediaName;
  String socialMediaLogo;
  String url;
  String createdOn;

  Data(
      {this.id,
        this.socialMediaName,
        this.socialMediaLogo,
        this.url,
        this.createdOn});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    socialMediaName = json['social_media_name'];
    socialMediaLogo = json['social_media_logo'];
    url = json['url'];
    createdOn = json['created_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['social_media_name'] = this.socialMediaName;
    data['social_media_logo'] = this.socialMediaLogo;
    data['url'] = this.url;
    data['created_on'] = this.createdOn;
    return data;
  }
}

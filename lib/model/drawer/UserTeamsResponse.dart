class UserTeamResponse {
  String status;
  List<Data> data;

  UserTeamResponse({this.status, this.data});

  UserTeamResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String memberId;
  String captainId;
  String gameId;
  String teamType;
  String name;
  String memberType;
  int tier;
  bool status;
  String createdOn;
  String updatedOn;

  Data(
      {this.id,
        this.memberId,
        this.captainId,
        this.gameId,
        this.teamType,
        this.name,
        this.memberType,
        this.tier,
        this.status,
        this.createdOn,
        this.updatedOn});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    memberId = json['member_id'];
    captainId = json['captain_id'];
    gameId = json['game_id'];
    teamType = json['team_type'];
    name = json['name'];
    memberType = json['member_type'];
    tier = json['tier'];
    status = json['status'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['member_id'] = this.memberId;
    data['captain_id'] = this.captainId;
    data['game_id'] = this.gameId;
    data['team_type'] = this.teamType;
    data['name'] = this.name;
    data['member_type'] = this.memberType;
    data['tier'] = this.tier;
    data['status'] = this.status;
    data['created_on'] = this.createdOn;
    data['updated_on'] = this.updatedOn;
    return data;
  }
}

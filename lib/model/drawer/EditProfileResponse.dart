class EditProfileResponse {
  String status;
  String message;
  ProfileData data;

  EditProfileResponse({this.status, this.message, this.data});

  EditProfileResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    message = json['message'];
    data = json['data'] != null ? new ProfileData.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['message'] = this.message;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class ProfileData {
  int userId;
  String firstName;
  String middleName;
  String lastName;
  String email;
  String phoneNumber;
  String dob;
  String address;
  bool isApproved;

  ProfileData(
      {this.userId,
        this.firstName,
        this.middleName,
        this.lastName,
        this.email,
        this.phoneNumber,
        this.dob,
        this.address,
        this.isApproved});

  ProfileData.fromJson(Map<String, dynamic> json) {
    userId = json['user_id'];
    firstName = json['first_name'];
    middleName = json['middle_name'];
    lastName = json['last_name'];
    email = json['email'];
    phoneNumber = json['phone_number'];
    dob = json['dob'];
    address = json['address'];
    isApproved = json['is_approved'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['user_id'] = this.userId;
    data['first_name'] = this.firstName;
    data['middle_name'] = this.middleName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['phone_number'] = this.phoneNumber;
    data['dob'] = this.dob;
    data['address'] = this.address;
    data['is_approved'] = this.isApproved;
    return data;
  }
}

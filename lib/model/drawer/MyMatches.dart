class MyMatches {
  String status;
  List<Data> data;

  MyMatches({this.status, this.data});

  MyMatches.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  Match match;
  List<Rounds> rounds;

  Data({this.match, this.rounds});

  Data.fromJson(Map<String, dynamic> json) {
    match = json['match'] != null ? new Match.fromJson(json['match']) : null;
    if (json['rounds'] != null) {
      rounds = new List<Rounds>();
      json['rounds'].forEach((v) {
        rounds.add(new Rounds.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.match != null) {
      data['match'] = this.match.toJson();
    }
    if (this.rounds != null) {
      data['rounds'] = this.rounds.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Match {
  int id;
  int gameId;
  String matchName;
  String matchType;
  String tier;
  String name;
  String profile;
  String cover;
  String entryFees;
  String startingAt;
  String registrationDateStart;
  String registrationDateLast;
  String createdOn;
  String updatedOn;
  bool status;

  Match(
      {this.id,
        this.gameId,
        this.matchName,
        this.matchType,
        this.tier,
        this.name,
        this.profile,
        this.cover,
        this.entryFees,
        this.startingAt,
        this.registrationDateStart,
        this.registrationDateLast,
        this.createdOn,
        this.updatedOn,
        this.status});

  Match.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    gameId = json['game_id'];
    matchName = json['match_name'];
    matchType = json['match_type'];
    tier = json['tier'];
    name = json['name'];
    profile = json['profile'];
    cover = json['cover'];
    entryFees = json['entry_fees'];
    startingAt = json['starting_at'];
    registrationDateStart = json['registration_date_start'];
    registrationDateLast = json['registration_date_last'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['game_id'] = this.gameId;
    data['match_name'] = this.matchName;
    data['match_type'] = this.matchType;
    data['tier'] = this.tier;
    data['name'] = this.name;
    data['profile'] = this.profile;
    data['cover'] = this.cover;
    data['entry_fees'] = this.entryFees;
    data['starting_at'] = this.startingAt;
    data['registration_date_start'] = this.registrationDateStart;
    data['registration_date_last'] = this.registrationDateLast;
    data['created_on'] = this.createdOn;
    data['updated_on'] = this.updatedOn;
    data['status'] = this.status;
    return data;
  }
}

class Rounds {
  int id;
  int matchId;
  String roundName;
  String matchDatetime;
  String roomId;
  String roomPassword;
  String liveStreamingLink;
  String youtubeStreamingLink;
  String facebookStreamingLink;
  bool isLocked;
  String createdOn;
  String updatedOn;

  Rounds(
      {this.id,
        this.matchId,
        this.roundName,
        this.matchDatetime,
        this.roomId,
        this.roomPassword,
        this.liveStreamingLink,
        this.youtubeStreamingLink,
        this.facebookStreamingLink,
        this.isLocked,
        this.createdOn,
        this.updatedOn});

  Rounds.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    matchId = json['match_id'];
    roundName = json['round_name'];
    matchDatetime = json['match_datetime'];
    roomId = json['room_id'];
    roomPassword = json['room_password'];
    liveStreamingLink = json['live_streaming_link'];
    youtubeStreamingLink = json['youtube_streaming_link'];
    facebookStreamingLink = json['facebook_streaming_link'];
    isLocked = json['is_locked'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['match_id'] = this.matchId;
    data['round_name'] = this.roundName;
    data['match_datetime'] = this.matchDatetime;
    data['room_id'] = this.roomId;
    data['room_password'] = this.roomPassword;
    data['live_streaming_link'] = this.liveStreamingLink;
    data['youtube_streaming_link'] = this.youtubeStreamingLink;
    data['facebook_streaming_link'] = this.facebookStreamingLink;
    data['is_locked'] = this.isLocked;
    data['created_on'] = this.createdOn;
    data['updated_on'] = this.updatedOn;
    return data;
  }
}

/// status : "success"
/// message : "Successfully registered new User"

class CommonResponse {
  String _status;
  String Message;

  String get status => _status;
  String get message => Message;

  CommonResponse({
      String status, 
      String message}){
    _status = status;
    Message = message;
}

  CommonResponse.fromJson(dynamic json) {
    _status = json["status"];
    Message = json["message"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = Message;
    return map;
  }

}
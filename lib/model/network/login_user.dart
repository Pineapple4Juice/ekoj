/// status : "success"
/// message : "Logged in Successfully"
/// data : {"user_id":10,"email":"rowan@gmail.com","first_name":"Rowan","middle_name":"","last_name":"Atkinson","phone_number":"9999999990"}

class LoginUser {
  String _status;
  String Message;
  LoginUserData _data;

  String get status => _status;
  String get message => Message;
  LoginUserData get data => _data;

  LoginUser({
      String status, 
      String message, 
      LoginUserData data}){
    _status = status;
    Message = message;
    _data = data;
}

  LoginUser.fromJson(dynamic json) {
    _status = json["status"];
    Message = json["message"];
    _data = json["data"] != null ? LoginUserData.fromJson(json["data"]) : null;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["status"] = _status;
    map["message"] = Message;
    if (_data != null) {
      map["data"] = _data.toJson();
    }
    return map;
  }

}

/// user_id : 10
/// email : "rowan@gmail.com"
/// first_name : "Rowan"
/// middle_name : ""
/// last_name : "Atkinson"
/// phone_number : "9999999990"

class LoginUserData {
  int _userId;
  String _email;
  String _firstName;
  String _middleName;
  String _lastName;
  String _phoneNumber;
  bool _isApproved;

  int get userId => _userId;
  String get email => _email;
  String get firstName => _firstName;
  String get middleName => _middleName;
  String get lastName => _lastName;
  String get phoneNumber => _phoneNumber;
  bool get isApproved => _isApproved;

  LoginUserData({
      int userId, 
      String email, 
      String firstName, 
      String middleName, 
      String lastName, 
      String phoneNumber,
  bool isApproved}){
    _userId = userId;
    _email = email;
    _firstName = firstName;
    _middleName = middleName;
    _lastName = lastName;
    _phoneNumber = phoneNumber;
    _isApproved = isApproved;
}

  LoginUserData.fromJson(dynamic json) {
    _userId = json["userId"];
    _email = json["email"];
    _firstName = json["firstName"];
    _middleName = json["middleName"];
    _lastName = json["lastName"];
    _phoneNumber = json["phoneNumber"];
    _isApproved = json["is_approved"];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map["userId"] = _userId;
    map["email"] = _email;
    map["firstName"] = _firstName;
    map["middleName"] = _middleName;
    map["lastName"] = _lastName;
    map["phoneNumber"] = _phoneNumber;
    map["is_approved"] = _isApproved;
    return map;
  }

}
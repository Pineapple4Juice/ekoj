class GameDetailResponse {
  String status;
  List<Data> data;

  String Message;

  GameDetailResponse({this.status, this.data});

  GameDetailResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  int gameName;
  String matchName;
  String matchType;
  String tier;
  String name;
  String profile;
  String cover;
  String entryFees;
  String totalNoOfSlots;
  String startingAt;
  String registrationDateStart;
  String registrationDateLast;
  String createdOn;
  String updatedOn;
  bool status;
  List<MatchRounds> matchRounds;

  Data(
      {this.id,
        this.gameName,
        this.matchName,
        this.matchType,
        this.tier,
        this.name,
        this.profile,
        this.cover,
        this.entryFees,
        this.totalNoOfSlots,
        this.startingAt,
        this.registrationDateStart,
        this.registrationDateLast,
        this.createdOn,
        this.updatedOn,
        this.status,
        this.matchRounds});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    gameName = json['game_name'];
    matchName = json['match_name'];
    matchType = json['match_type'];
    tier = json['tier'];
    name = json['name'];
    profile = json['profile'];
    cover = json['cover'];
    entryFees = json['entry_fees'];
    totalNoOfSlots = json['total_no_of_slots'];
    startingAt = json['starting_at'];
    registrationDateStart = json['registration_date_start'];
    registrationDateLast = json['registration_date_last'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    status = json['status'];
    if (json['match_rounds'] != null) {
      matchRounds = new List<MatchRounds>();
      json['match_rounds'].forEach((v) {
        matchRounds.add(new MatchRounds.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['game_name'] = this.gameName;
    data['match_name'] = this.matchName;
    data['match_type'] = this.matchType;
    data['tier'] = this.tier;
    data['name'] = this.name;
    data['profile'] = this.profile;
    data['cover'] = this.cover;
    data['entry_fees'] = this.entryFees;
    data['total_no_of_slots'] = this.totalNoOfSlots;
    data['starting_at'] = this.startingAt;
    data['registration_date_start'] = this.registrationDateStart;
    data['registration_date_last'] = this.registrationDateLast;
    data['created_on'] = this.createdOn;
    data['updated_on'] = this.updatedOn;
    data['status'] = this.status;
    if (this.matchRounds != null) {
      data['match_rounds'] = this.matchRounds.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class MatchRounds {
  int id;
  String roundName;
  String matchDatetime;
  String roomId;
  String roomPassword;
  String liveStreamingLink;
  String youtubeStreamingLink;
  String facebookStreamingLink;
  bool isLocked;
  String createdOn;
  String updatedOn;
  int match;

  MatchRounds(
      {this.id,
        this.roundName,
        this.matchDatetime,
        this.roomId,
        this.roomPassword,
        this.liveStreamingLink,
        this.youtubeStreamingLink,
        this.facebookStreamingLink,
        this.isLocked,
        this.createdOn,
        this.updatedOn,
        this.match});

  MatchRounds.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    roundName = json['round_name'];
    matchDatetime = json['match_datetime'];
    roomId = json['room_id'];
    roomPassword = json['room_password'];
    liveStreamingLink = json['live_streaming_link'];
    youtubeStreamingLink = json['youtube_streaming_link'];
    facebookStreamingLink = json['facebook_streaming_link'];
    isLocked = json['is_locked'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
    match = json['match'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['round_name'] = this.roundName;
    data['match_datetime'] = this.matchDatetime;
    data['room_id'] = this.roomId;
    data['room_password'] = this.roomPassword;
    data['live_streaming_link'] = this.liveStreamingLink;
    data['youtube_streaming_link'] = this.youtubeStreamingLink;
    data['facebook_streaming_link'] = this.facebookStreamingLink;
    data['is_locked'] = this.isLocked;
    data['created_on'] = this.createdOn;
    data['updated_on'] = this.updatedOn;
    data['match'] = this.match;
    return data;
  }
}
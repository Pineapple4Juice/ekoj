class UserData {
  String status;
  Data data;

  UserData({this.status, this.data});

  UserData.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int id;
  String firstName;
  String middleName;
  String lastName;
  String email;
  String phoneNumber;
  String dob;
  String gender;
  String profilePic;
  String address;
  String otp;
  String depositAmount;
  String withdrawAmount;
  String currency;
  String loginType;
  String socialToken;
  String timeZone;
  bool isActive;
  String createdAt;
  String updatedAt;
  int user;

  Data(
      {this.id,
        this.firstName,
        this.middleName,
        this.lastName,
        this.email,
        this.phoneNumber,
        this.dob,
        this.gender,
        this.profilePic,
        this.address,
        this.otp,
        this.depositAmount,
        this.withdrawAmount,
        this.currency,
        this.loginType,
        this.socialToken,
        this.timeZone,
        this.isActive,
        this.createdAt,
        this.updatedAt,
        this.user});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    firstName = json['first_name'];
    middleName = json['middle_name'];
    lastName = json['last_name'];
    email = json['email'];
    phoneNumber = json['phone_number'];
    dob = json['dob'];
    gender = json['gender'];
    profilePic = json['profile_pic'];
    address = json['address'];
    otp = json['otp'];
    depositAmount = json['deposit_amount'];
    withdrawAmount = json['withdraw_amount'];
    currency = json['currency'];
    loginType = json['login_type'];
    socialToken = json['social_token'];
    timeZone = json['time_zone'];
    isActive = json['is_active'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    user = json['user'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['first_name'] = this.firstName;
    data['middle_name'] = this.middleName;
    data['last_name'] = this.lastName;
    data['email'] = this.email;
    data['phone_number'] = this.phoneNumber;
    data['dob'] = this.dob;
    data['gender'] = this.gender;
    data['profile_pic'] = this.profilePic;
    data['address'] = this.address;
    data['otp'] = this.otp;
    data['deposit_amount'] = this.depositAmount;
    data['withdraw_amount'] = this.withdrawAmount;
    data['currency'] = this.currency;
    data['login_type'] = this.loginType;
    data['social_token'] = this.socialToken;
    data['time_zone'] = this.timeZone;
    data['is_active'] = this.isActive;
    data['created_at'] = this.createdAt;
    data['updated_at'] = this.updatedAt;
    data['user'] = this.user;
    return data;
  }
}

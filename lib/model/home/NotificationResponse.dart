class NotificationResponse {
  String status;
  List<Data> data;

  NotificationResponse({this.status, this.data});

  NotificationResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  int id;
  String notificationFor;
  String title;
  String description;
  bool status;
  String createdOn;
  int userId;

  Data(
      {this.id,
        this.notificationFor,
        this.title,
        this.description,
        this.status,
        this.createdOn,
        this.userId});

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    notificationFor = json['notification_for'];
    title = json['title'];
    description = json['description'];
    status = json['status'];
    createdOn = json['created_on'];
    userId = json['user_id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['notification_for'] = this.notificationFor;
    data['title'] = this.title;
    data['description'] = this.description;
    data['status'] = this.status;
    data['created_on'] = this.createdOn;
    data['user_id'] = this.userId;
    return data;
  }
}

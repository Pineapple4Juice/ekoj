class AvailableGames {
    List<Data> data;
    String status;

    AvailableGames({this.data, this.status});

    factory AvailableGames.fromJson(Map<String, dynamic> json) {
        return AvailableGames(
            data: json['data'] != null ? (json['data'] as List).map((i) => Data.fromJson(i)).toList() : null, 
            status: json['status'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['status'] = this.status;
        if (this.data != null) {
            data['data'] = this.data.map((v) => v.toJson()).toList();
        }
        return data;
    }
}

class Data {
    String cover_pic;
    String created_on;
    String game_type;
    int id;
    bool is_active;
    String name;
    String profile_pic;
    String updated_on;

    Data({this.cover_pic, this.created_on, this.game_type, this.id, this.is_active, this.name, this.profile_pic, this.updated_on});

    factory Data.fromJson(Map<String, dynamic> json) {
        return Data(
            cover_pic: json['cover_pic'], 
            created_on: json['created_on'], 
            game_type: json['game_type'], 
            id: json['id'], 
            is_active: json['is_active'], 
            name: json['name'], 
            profile_pic: json['profile_pic'], 
            updated_on: json['updated_on'], 
        );
    }

    Map<String, dynamic> toJson() {
        final Map<String, dynamic> data = new Map<String, dynamic>();
        data['cover_pic'] = this.cover_pic;
        data['created_on'] = this.created_on;
        data['game_type'] = this.game_type;
        data['id'] = this.id;
        data['is_active'] = this.is_active;
        data['name'] = this.name;
        data['profile_pic'] = this.profile_pic;
        data['updated_on'] = this.updated_on;
        return data;
    }
}
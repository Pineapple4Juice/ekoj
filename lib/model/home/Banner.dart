
List<BannerClass> parseBannerResponse(json)  {
  return json != null ? (json as List).map((i) => BannerClass.fromJson(i)).toList() : null;
}


class BannerClass {
  int id;
  String profilePic;
  bool isActive;
  String createdOn;
  String updatedOn;

  BannerClass(
      {this.id,
        this.profilePic,
        this.isActive,
        this.createdOn,
        this.updatedOn});

  BannerClass.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    profilePic = json['profile_pic'];
    isActive = json['is_active'];
    createdOn = json['created_on'];
    updatedOn = json['updated_on'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['profile_pic'] = this.profilePic;
    data['is_active'] = this.isActive;
    data['created_on'] = this.createdOn;
    data['updated_on'] = this.updatedOn;
    return data;
  }
}

import 'package:flutter/material.dart';

Widget streamConsumer<T>(AsyncSnapshot<T> snapshot, Widget Function() hasData) {
  if (snapshot.hasData && snapshot.data != null) {
    return hasData();
  } else if (snapshot.hasError) {
    return Center(child: Text("Sorry, Something Went Wrong"));
  } else {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
          child: CircularProgressIndicator(
        valueColor: AlwaysStoppedAnimation(Colors.indigo),
      )),
    );
  }
}

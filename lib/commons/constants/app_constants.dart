class AppConstants {
  AppConstants._();

  // base url
  static const String domain = "http://ekoj.pythonanywhere.com/";
  static const String baseUrl = "${domain}api/";

  // receiveTimeout
  static const int receiveTimeout = 30000;

  // connectTimeout
  static const int connectionTimeout = 30000;

  // booking endpoints
  static const String REGISTER = 'account/register/';
  static const String LOGIN = 'account/login/';
  static const String Banner='account/banners';
  static const String AVAILABLE_GAMES='account/games/';
  static const String CUSTOMER_SUPPORT='account/customer_support/?user_id=5';
  static const String UPCOMING = 'account/matches/upcoming/';
  static const String HIRERESULT='account/HireResult/';
  static const String JOINMATCH='account/match/join/';
  static const String MATCHJOINEDTEAM='account/match_joined_teams/';
  static const String GETPROFILE='account/user_profile/';
  static const String EDITPROFILE='account/editprofile/';
  static const String NOTIFICATIONS='account/notifications/';
  static const String MYMATCHES='account/mymatches/';
  static const String USERTEAMS='account/teams/';



  //Preferences
  static const String AUTH_TOKEN = "AUTH_TOKEN";
}

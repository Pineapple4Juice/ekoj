import 'dart:convert';


import 'package:ekojpubg/model/network/login_user.dart';
import 'package:shared_preferences/shared_preferences.dart';




class SharedPreferenceUtil {
  SharedPreferenceUtil._();

 static Future<LoginUserData> getUserDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final userJsonString = prefs.getString("user");
    if (userJsonString != null && userJsonString.isNotEmpty) {
      var loginJson = await json.decode(prefs.getString("user"));
      return LoginUserData.fromJson(loginJson);
    } else {
      return null;
    }
  }


  static Future<bool> clearPreference() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.clear();
  }

  static Future<bool> saveUser(LoginUserData userData) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("user", json.encode(userData.toJson()));
  }

  static Future<bool> saveUserId(String userId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString("userId", userId);
  }

  static Future<String> getUserid() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    final userId = prefs.getString("userId");
    return userId;
  }

  static Future<bool> logoutUser() {
   return clearPreference();
  }
}

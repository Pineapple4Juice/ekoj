import 'dart:io';

import 'package:ekojpubg/app/app_theme.dart';
import 'package:ekojpubg/app/size_config.dart';
import 'package:ekojpubg/utils/app_utils.dart';
import 'package:ekojpubg/widgets/base_appbar.dart';
import 'package:ekojpubg/widgets/progress_hud.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'base_screen.dart';
import 'base_stateful_widget.dart';

abstract class BaseState<Page extends BaseStatefulWidget> extends State<Page>
    with BaseScreen {
  bool _isLoading = false;
  BuildContext context;
  //Config _config;
  bool defaultPadding;

  BaseState({this.defaultPadding = true});

  @override
  void initState() {
    super.initState();
    /*  WidgetsBinding.instance
        .addPostFrameCallback((_) => checkAppEnvironment());*/
  }

  @override
  Widget build(BuildContext context) {
    this.context = context;
   // _config = AppConfig.of(context);
    SizeConfig().init(context);
    /*print("::::::::ENV:::::::::" + _config.env);
    print("::::::::ENV:BaseAPI:::::::::" + _config.baseApi);
    */return ProgressHUD(
        inAsyncCall: _isLoading,
        opacity: 0.5,
        child: useBaseLayout()
            ? setBaseTabLayout() > 0
                ? getTabLayout(setBaseTabLayout())
                : getBaseLayout()
            : buildBody(context));
  }

  @override
  PreferredSizeWidget setBottomWidget() => null;

  @override
  bool useBaseLayout() => true;

  @override
  int setBaseTabLayout() => 0;

  @override
  Color setBackgroundColor() => AppTheme.pageBackground;

  @override
  bool showDrawer() => false;

  @override
  List<Widget> setAppbarWidgets() => null;

  /// show progress bar
  void showProgress() {
    setState(() {
      _isLoading = true;
    });
  }

  /// hide progress bar
  void hideProgress() {
    setState(() {
      _isLoading = false;
    });
  }

  @override
  void onHideProgress() {
    hideProgress();
  }

  @override
  void onShowProgress() {
    showProgress();
  }

  @override
  void onError(Object e, {StackTrace stackTrace}) {
    hideProgress();
    if (stackTrace != null) print(stackTrace);
    if (e is SocketException) {
      showMessage('No Network Avaiable');
    } else {
      showMessage('Unexpected Error Occured');
    }
  }

  /*@override
  getAppConfig() {
    return _config;
  }*/

  Widget getBaseLayout() {
    return Scaffold(
        appBar: screenName() != null
            ? BaseAppbar(
                title: screenName(),
                appBar: AppBar(),
                widgets: setAppbarWidgets(),
                bottom: setBottomWidget())
            : null,
        //TODO
        //implement drawer
        //drawer: showDrawer() ? HomeDrawer() : null,
        body: Container(
            color: setBackgroundColor(),
            padding: defaultPadding ?AppTheme.main_block_padding: EdgeInsets.all(0.0),
            child: buildBody(context)));
  }

  Widget getTabLayout(int tabCount) {
    return DefaultTabController(
        // The number of tabs / content sections to display.
        length: tabCount,
        child: getBaseLayout());
  }

  /// show message: toast on android or alert dialog on ios
  void showMessage(String message) {
    if (message == null) {
      return;
    }
    if (Platform.isIOS) {
      showDialog(
        context: context,
        builder: (BuildContext context) => CupertinoAlertDialog(
          content: Text(message),
          //title: Text(''),
          actions: [
            CupertinoDialogAction(
              child: new Text('Ok'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        ),
      );
    } else {
      Fluttertoast.showToast(
          msg: message,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1,
          textColor: Colors.white,
          fontSize: 14.0);
    }
  }

  void showAlertDialog(String message, Function action, bool isDismissible) {
    if (message == null) {
      return;
    }

    showDialog(
      context: context,
      barrierDismissible: isDismissible,
      builder: (BuildContext context) => CupertinoAlertDialog(
        content: Text(message),
        //title: Text(''),
        actions: [
          CupertinoDialogAction(
            child: new Text('Ok'),
            onPressed: action,
          )
        ],
      ),
    );
  }

  checkAppEnvironment() async {
    String alertMessage;
    bool isAlertDismissable = true;
    bool isExitAction = false;
    //await AppUtils.isDeviceRooted();
    bool isRooted = false; //await AppUtils.isDeviceRooted();
    if (isRooted) {
      //if(await AppUtils.isDeviceRooted()){
      alertMessage =
          "Your Device is Rooted. Application will not support in Rooted Devices.";
      isAlertDismissable = false;
      isExitAction = true;
    } else if (!await AppUtils.isNetworkAvailable()) {
      alertMessage = "No Network Detected. Please check your NetworkSettings.";
    }
    setState(() {
      showAlertDialog(alertMessage, () {
        if (isExitAction) {
          if (Platform.isIOS) {
            exit(0);
          } else {
            SystemNavigator.pop();
          }
        } else {
          Navigator.of(context).pop();
        }
      }, isAlertDismissable);
    });
  }
}

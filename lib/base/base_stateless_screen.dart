import 'package:ekojpubg/app/app_theme.dart';
import 'package:ekojpubg/app/size_config.dart';
import 'package:ekojpubg/widgets/base_appbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'base_screen.dart';
import 'base_stateless_widget.dart';

abstract class BaseStatelessScreen extends BaseStatelessWidget with BaseScreen {
  BuildContext context;
 // Config _config;

  @override
  Widget getLayout(BuildContext context) {
    this.context = context;
   // _config = AppConfig.of(context);
    SizeConfig().init(context);
    return useBaseLayout()
        ? setBaseTabLayout() > 0
            ? getTabLayout(setBaseTabLayout())
            : getBaseLayout()
        : buildBody(context);
  }

  @override
  PreferredSizeWidget setBottomWidget() => null;

  @override
  bool useBaseLayout() => true;

  @override
  int setBaseTabLayout() => 0;

  @override
  Color setBackgroundColor() => AppTheme.pageBackground;

  @override
  void onHideProgress() {}

  @override
  void onShowProgress() {}

  @override
  List<Widget> setAppbarWidgets() => null;

  @override
  bool showDrawer() => false;

  @override
  void onError(Object e) {
    // TODO: implement onError
  }

  Widget getBaseLayout() {
    return Scaffold(
        appBar: BaseAppbar(
            title: screenName(),
            appBar: AppBar(),
            widgets: setAppbarWidgets(),
            bottom: setBottomWidget()),
        //TODO
        //implement drawer
        //drawer: showDrawer() ? HomeDrawer() : null,
        body: Container(
            color: setBackgroundColor(),
            padding: AppTheme.main_block_padding,
            child: buildBody(context)));
  }

  Widget getTabLayout(int tabCount) {
    return DefaultTabController(
        // The number of tabs / content sections to display.
        length: tabCount,
        child: getBaseLayout());
  }

  Widget loadingScreen() {
    return new Stack(
      children: [
        new Opacity(
          opacity: 0.5,
          child: ModalBarrier(dismissible: false, color: Colors.white),
        ),
        new Center(
          child: new CircularProgressIndicator(
              //valueColor: Colors.black,
              ),
        ),
      ],
    );
    // widgetList.add(modal);
  }

  /*@override
  getAppConfig() {
    return _config;
  }*/
}

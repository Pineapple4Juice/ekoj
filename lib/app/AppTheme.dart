import 'package:flutter/material.dart';

class AppTheme {
  static get() {
    return ThemeData(
      // Define the default brightness and colors.
      brightness: Brightness.light,
      primaryColor: Color(0xff35465d),
      accentColor: Colors.red,
      primaryColorDark: Color(0xff35465d),

      // Define the default font family.
      fontFamily: 'Quicksand',
      visualDensity: VisualDensity.adaptivePlatformDensity,
      /*textTheme: TextTheme(
        headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
        headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
        bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
      )*/
    );
  }

  static getPrimaryColor(BuildContext context) {
    return Theme.of(context).primaryColor;
  }

  static getAccentColor(BuildContext context) {
    return Theme.of(context).accentColor;
  }

  static get getLightPurple => Color(0xffAD81B4);
  static get getCreamePurple => Color(0xff8E5E9D);
  static get getAppBlack => Color(0xff1D1D1B);
  static get getTextGrey => Color(0xff7c7c7c);
}

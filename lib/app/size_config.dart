import 'dart:math';

import 'package:flutter/widgets.dart';

class SizeConfig {
  /*static MediaQueryData _mediaQueryData;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;

  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;*/

  static MediaQueryData _mediaQueryData;
  static double _screenWidth;
  static double _factorHorizontal;
  static double _screenHeight;
  static double _factorVertical;
  static double _textScalingFactor;
  static double _safeAreaHorizontal;
  static double _safeFactorHorizontal;
  static double _safeAreaVertical;
  static double _safeFactorVertical;
  static double _safeAreaTextScalingFactor;

  void init(BuildContext context) {
    /*_mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;

    _safeAreaHorizontal = _mediaQueryData.padding.left +
        _mediaQueryData.padding.right;
    _safeAreaVertical = _mediaQueryData.padding.top +
        _mediaQueryData.padding.bottom;
    safeBlockHorizontal = (screenWidth -
        _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight -
        _safeAreaVertical) / 100;*/
    final _divisor = 400.0;

    _mediaQueryData = MediaQuery.of(context);

    _screenWidth = _mediaQueryData.size.width;
    _factorHorizontal = _screenWidth / _divisor;

    _screenHeight = _mediaQueryData.size.height;
    _factorVertical = _screenHeight / _divisor;

    _textScalingFactor = min(_factorVertical, _factorHorizontal);

    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeFactorHorizontal = (_screenWidth - _safeAreaHorizontal) / _divisor;

    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;
    _safeFactorVertical = (_screenHeight - _safeAreaVertical) / _divisor;

    _safeAreaTextScalingFactor =
        min(_safeFactorHorizontal, _safeFactorHorizontal);
  }

  static double getScaleFactor() {
    return _safeAreaTextScalingFactor;
  }
}

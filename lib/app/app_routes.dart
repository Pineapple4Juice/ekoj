
import 'package:ekojpubg/screens/bookSlotDetail/BookSlotDetailScreen.dart';
import 'package:ekojpubg/screens/createTeam/CreateTeamScreen.dart';
import 'package:ekojpubg/screens/customer_support/CustomerSupportScreen.dart';
import 'package:ekojpubg/screens/gameDetail/GameDetailScreen.dart';
import 'package:ekojpubg/screens/gameDetail/GameItemDetailScreen.dart';
import 'package:ekojpubg/screens/hire_result/HireAndResultScreen.dart';
import 'package:ekojpubg/screens/home/home_screen.dart';
import 'package:ekojpubg/screens/login/login.dart';
import 'package:ekojpubg/screens/mobilescrims/pubg_mobile_scrims.dart';
import 'package:ekojpubg/screens/mobilescrims/slot_pubg_scrim.dart';
import 'package:ekojpubg/screens/myMatch/my_matches.dart';
import 'package:ekojpubg/screens/notification/notification.dart';
import 'package:ekojpubg/screens/profile/profile_screen.dart';
import 'package:ekojpubg/screens/register/signup.dart';
import 'package:ekojpubg/screens/sponsMatch/sponsored_matches.dart';
import 'package:ekojpubg/screens/wallet/Transactions_history_screen.dart';
import 'package:ekojpubg/screens/wallet/wallet_view.dart';
import 'package:ekojpubg/screens/wallet/withdraw_view.dart';
import 'package:flutter/material.dart';

class Router {
  static Map<String, WidgetBuilder> routes() {
    return {
      LoginView.routeName:(ctx)=> LoginView(),
      SignUpView.routeName:(ctx)=> SignUpView(),
      NotificationScreen.routeName:(ctx)=>NotificationScreen(),
      HomeScreen.routeName:(ctx)=>HomeScreen(),
      SponseredMatchesScreen.routeName:(ctx)=>SponseredMatchesScreen(),
      MyMatchesScreen.routeName:(ctx)=>MyMatchesScreen(),
      ProfileScreen.routeName:(ctx)=> ProfileScreen(),
      GameDetailScreen.routeName:(ctx)=>GameDetailScreen(),
      GameItemDetailScreen.routeName:(ctx)=>GameItemDetailScreen(),
      PubgMobileScrims.routeName:(ctx)=>PubgMobileScrims(),
      WalletViewScreen.routeName:(ctx)=>WalletViewScreen(),
      TransactionScreen.routeName:(ctx)=>TransactionScreen(),
      BookSlotDetailScreen.routeName:(ctx)=>BookSlotDetailScreen(),
      SlotPubgScrimScreen.routeName:(ctx)=>SlotPubgScrimScreen(),
      HireAndResultScreen.routeName:(ctx)=>HireAndResultScreen(),
      CreateTeamScreen.routeName:(ctx)=>CreateTeamScreen(),
      CustomerSupportScreen.routeName:(ctx)=>CustomerSupportScreen(),
      WithDrawScreen.routeName:(ctx)=>WithDrawScreen()
      //DashboardScreen.routeName: (ctx) => DashboardScreen(),
     // SettingsScreen.routeName: (ctx) => SettingsScreen(),
     // LoginScreen.routeName: (ctx) => LoginScreen(),
    };
  }
}

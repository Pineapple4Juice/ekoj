import 'package:dio/dio.dart';
import 'package:ekojpubg/commons/constants/app_constants.dart';


import 'interceptor/LoggingInterceptor.dart';

class ApiHitter {
  static Dio _dio;

  static Dio getDio() {
    if (_dio == null) {
      BaseOptions options = new BaseOptions(
          baseUrl: AppConstants.baseUrl, //_config.baseApi, //
          connectTimeout: AppConstants.connectionTimeout,
          receiveTimeout: AppConstants.receiveTimeout);
      _dio = new Dio(options)
        ..interceptors.add(LoggingInterceptor(requestBody: true));
      return _dio;
    } else {
      return _dio;
    }
  }

  static Future<ApiResponse> getApiResponse(String endPoint,
      {Map<String, dynamic> headers,
      Map<String, dynamic> queryParameters}) async {
    try {
      Response response = await getDio().get(endPoint,
          options: Options(headers: headers), queryParameters: queryParameters);
      return ApiResponse(true, response: response);
    } catch (error, stacktrace) {
      try {
        return ApiResponse(false,
            msg: "${error.response.data['message'] ?? "Sorry Something went wrong."}");
      } catch (e) {
        return ApiResponse(false, msg: "Sorry Something went wrong.");
      }
    }
  }

  static Future<ApiResponse> getPostApiResponse(String endPoint,
      {Map<String, dynamic> headers, Map<String, dynamic> data, FormData formData}) async {
    try {
      var response = await getDio()
          .post(endPoint, options: Options(headers: headers), data: formData==null?data:formData);
      return ApiResponse(true, response: response, msg: "success");
    } catch (error, stacktrace) {
      try {
        return ApiResponse(false,
            msg: "${error.response.data['message'] ?? "Sorry Something went wrong."}");
      } catch (e) {
        return ApiResponse(false, msg: "Sorry Something went wrong.");
      }
    }
  }
}

class ApiResponse {
  final bool status;
  final String msg;
  final Response response;

  ApiResponse(this.status, {this.msg = "success", this.response});
}

import 'package:flutter/material.dart';

class CustomDropDown extends StatefulWidget {
  bool showDivider;

  List<String> options;
  String hint;
  String selectedItem;
  Function(String) getSelectedItem;

  @override
  _CustomDropDownState createState() => _CustomDropDownState();

  CustomDropDown(this.options, this.hint, this.selectedItem, this.showDivider,
      this.getSelectedItem);
}

class _CustomDropDownState extends State<CustomDropDown> {
  String selectedItem;

  @override
  void initState() {
    super.initState();
    if (widget.selectedItem != null) {
      this.selectedItem = widget.selectedItem;
    }
  }

  @override
  Widget build(BuildContext context) {
    return setSearchDropDown(widget.options, widget.hint);
  }

  Widget setSearchDropDown(List<String> list, hint) {
    return list.length > 0
        ? DropdownButton<String>(
            hint: Text(hint),
            isExpanded: true,
            value: selectedItem,
            icon: Icon(Icons.expand_more),
            iconSize: 20,
            underline: widget.showDivider == false ? SizedBox() : null,
            onChanged: (value) {
              setState(() {
                this.selectedItem = value;
                widget.getSelectedItem(value);
              });
            },
            items: list.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(
                  value,
                  style: TextStyle(color: Color(0xFF78849E), fontSize: 14.0),
                ),
              );
            }).toList(),
          )
        : null;
  }
}

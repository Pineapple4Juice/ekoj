import 'package:flutter/material.dart';

class BorderButton extends StatelessWidget {
  final Function _onPressAction;
  final String _title;
  final Color borderColor;
  final Color background;
  final Color buttonTextColor;

  BorderButton(this._onPressAction, this._title, this.borderColor,
      this.background, this.buttonTextColor);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(
        _title,
        style: TextStyle(fontSize: 14.0),
      ),
      onPressed: _onPressAction,
      color: background,
      textColor: buttonTextColor,
      padding: EdgeInsets.fromLTRB(10, 14, 10, 14),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8.0),
          side: BorderSide(color: borderColor, width: 1.0)),
    );
  }
}

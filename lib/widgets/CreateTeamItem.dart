import 'package:ekojpubg/model/Player.dart';
import 'package:ekojpubg/model/drawer/MatchJoinedTeamResponse.dart';
import 'package:ekojpubg/model/drawer/UserTeamsResponse.dart';
import 'package:ekojpubg/utils/extensions/basic_extensions.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';

class CreateTeamItem extends StatefulWidget {
  final String type;
  final VoidCallback callback;
  final List<Data> userTeam;

  CreateTeamItem(this.type,
      {this.userTeam,
        this.callback});

  @override
  _CreateTeamItemState createState() => _CreateTeamItemState();
}

class _CreateTeamItemState extends State<CreateTeamItem> {
  List<Data> teamMembers;
  @override
  void initState() {
    super.initState();
    this.widget.userTeam!=null && this.widget.userTeam.length>0 ?
    teamMembers=this.widget.userTeam :teamMembers= null;
  }
  @override
  Widget build(BuildContext context) {

    return  Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      child: Stack(
        children: <Widget>[
          Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                height: 37,
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.black87,
                    borderRadius: BorderRadius.circular(12)),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: Text(
                      widget.type,
                      style: TextStyle(color: Colors.white, fontSize: 12),
                    ),
                  ),
                ),
              )),
          Align(
            alignment: Alignment.topCenter,
            child: SizedBox(
              height: 80,
              child: Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[SizedBox(width: 16)]..addAll(teamMembers
                    .map((e) => InkWell(onTap: () {
                  widget?.callback?.call();
                },child: getProfileImage(e).paddingRight(16)))
                    .toList()),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget getProfileImage(Data users) => Column(
    children: <Widget>[
      Container(
        height: 30,
        width: 30,
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          border: Border.all(color: Colors.black87, width: 1),
        ),
        child: ClipRRect(
            borderRadius: BorderRadius.circular(50),
            child: Image.asset(
              "assets/images/team_profile.png",
              height: 29,
              width: 29,
              fit: BoxFit.cover,
            )),
      ),
      Text(users.name,style: TextStyle(color: Colors.white,fontSize: 14.w),)
    ],
  );
}

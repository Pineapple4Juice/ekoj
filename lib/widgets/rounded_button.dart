import 'package:flutter/material.dart';

import '../app/app_theme.dart';

class RoundedCornerButton extends StatelessWidget {
  final Function _onPressAction;
  final String _title;
  final Color backgroundColor;
  final Color textColor;

  RoundedCornerButton(this._onPressAction, this._title,{this.backgroundColor, this.textColor});
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(
        _title,
        style: TextStyle(fontSize: 18),
      ),
      onPressed: _onPressAction,
      textColor: textColor!=null?textColor:AppTheme.white,
      color:  backgroundColor!=null?backgroundColor:Colors.black,
      padding: EdgeInsets.fromLTRB(30, 10, 30, 10),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(32.0)),
    );
  }
}

import 'package:flutter/material.dart';

import '../app/app_localizations.dart';

class PasswordFiled extends StatelessWidget {
  final TextEditingController controller;

  PasswordFiled(this.controller);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: this.controller,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: AppTranslations.of(context).text("password"),
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );
  }
}

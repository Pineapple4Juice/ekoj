import 'package:flutter/material.dart';

import '../app/app_theme.dart';

class RectangularShapeButton extends StatelessWidget {
  final Function _onPressAction;
  final String _title;

  RectangularShapeButton(
    this._onPressAction,
    this._title,
  );

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text(
        _title,
        style: TextStyle(fontSize: 16),
      ),
      onPressed: _onPressAction,
      color: Color(0xFFFE4365),
      //AppTheme.secondaryAppThemeColor,
      textColor: AppTheme.white,
      padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
    );
  }
}

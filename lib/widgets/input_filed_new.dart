import 'package:flutter/material.dart';

class SimpleInputFiled extends StatelessWidget {
  final String hintText;
  final String defaultText;
  final bool isSecureInput;
  final bool prefixIconBoolValue;

  final TextEditingController textController;

  SimpleInputFiled(
      {this.hintText,
      this.isSecureInput,
      this.textController,
      this.defaultText,
      this.prefixIconBoolValue});

  @override
  Widget build(BuildContext context) {
    return TextField(
      obscureText: isSecureInput,
      controller: textController,
      style: TextStyle(
          color: Colors.white, fontSize: 15, fontWeight: FontWeight.w500),
      decoration: InputDecoration(
        prefixIcon: Padding(
          //padding: const EdgeInsets.only(bottom: 5.0, left: 0),
          padding: const EdgeInsetsDirectional.only(bottom: 5.0),
          child: Icon(
            prefixIconBoolValue ? Icons.person : Icons.lock_open,
            color: Colors.white,
            size: 23,
          ),
        ),
        hintText: hintText,
        hintStyle: TextStyle(color: Colors.white),
        border: new UnderlineInputBorder(
          borderSide: BorderSide(
              color: Colors.white, width: 1.0, style: BorderStyle.none),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
        ),
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(
            color: Colors.white,
            width: 0.3,
          ),
        ),
      ),
    );
  }
}

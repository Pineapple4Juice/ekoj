import 'package:flutter/material.dart';
import 'package:ekojpubg/utils/responsive/size_extension.dart';

class SmallRoundedCornerButton extends StatelessWidget {
  final Function _onPressAction;
  final String _title;

  SmallRoundedCornerButton(this._onPressAction, this._title);

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      child: Text(
        _title,
        style: TextStyle(fontSize: 14.0.sp,
            letterSpacing: 0.8),
      ),
      onPressed: _onPressAction,
      textColor: Colors.white,
      color: Colors.black,
      padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(40.0)),
    );
  }
}
import 'package:catcher/catcher_plugin.dart';
import 'package:ekojpubg/app/AppTheme.dart';
import 'package:ekojpubg/app/app_routes.dart';
import 'package:ekojpubg/screens/hire_result/HireAndResultScreen.dart';
import 'package:ekojpubg/screens/mobilescrims/slot_pubg_scrim.dart';
import 'package:ekojpubg/screens/splash/splash.dart';
import 'package:ekojpubg/screens/wallet/wallet_view.dart';
import 'package:flutter/material.dart';

Catcher catcher;

void main() {

  return runApp(MaterialApp(
          theme: AppTheme.get(),
          navigatorKey: Catcher.navigatorKey,
          routes: Router.routes(),
          debugShowCheckedModeBanner: false,
          home: SplashScreen()));

  /*runApp(MyApp());*/
}

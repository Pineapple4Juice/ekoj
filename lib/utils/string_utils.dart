class StringUtils {
  StringUtils();

  static formartasposition(int number) {
    String suffix = 'th';
    final int digit = number % 10;
    if ((digit > 0 && digit < 4) && (number < 11 || number > 13)) {
      suffix = <String>['st', 'nd', 'rd'][digit - 1];
    }
    return "$number$suffix";
  }
}

import 'package:connectivity/connectivity.dart';
import 'package:intl/intl.dart';
import 'package:trust_fall/trust_fall.dart';

class AppUtils {
  AppUtils._();

  static Future<bool> isNetworkAvailable() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  static Future<bool> isDeviceRooted() async {
    try {
      return await TrustFall.isTrustFall;
    } catch (error) {
      print(error);
    }
    return false;
  }

  static String formatDate(date){
    DateFormat formatter, FORMATTER;
    formatter = new DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz");
    String oldDate = "2008-03-14T11:53:31.200+05:00";
    DateTime date = formatter.parse(oldDate.substring(0, 26) + oldDate.substring(27, 29));
    FORMATTER = new DateFormat("MM/dd/yyyy");


    return FORMATTER.format(date);
  }

  static String formatTime(date){
    DateFormat formatter, FORMATTER;
    formatter = new DateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSz");
    String oldDate = "2008-03-14T11:53:31.200+05:00";
    DateTime date = formatter.parse(oldDate.substring(0, 26) + oldDate.substring(27, 29));
    FORMATTER = new DateFormat("hh:mm a");


    return FORMATTER.format(date);
  }
}

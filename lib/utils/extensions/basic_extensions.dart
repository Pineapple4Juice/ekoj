
import 'package:ekojpubg/widgets/NoScrollEffectScrollBehaviour.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

extension BasicWidgetExtensions on Widget {
  Widget clip(CustomClipper<Path> customClipper) {
    return ClipPath(
        clipper: customClipper, clipBehavior: Clip.antiAlias, child: this);
  }

  Widget paddingE(EdgeInsets insets) {
    return Padding(padding: insets, child: this);
  }


  Widget noScrollEffect() {
    return ScrollConfiguration(
        behavior: NoScrollEffectScrollBehaviour(), child: this);
  }


  Widget paddingTop(double padding) {
    return Padding(padding: EdgeInsets.only(top: padding), child: this);
  }

  Widget paddingLeft(double padding) {
    return Padding(padding: EdgeInsets.only(left: padding), child: this);
  }

  Widget paddingRight(double padding) {
    return Padding(padding: EdgeInsets.only(right: padding), child: this);
  }

  Widget paddingBottom(double padding) {
    return Padding(padding: EdgeInsets.only(bottom: padding), child: this);
  }

  Widget paddingAll({double top = 0, double left = 0, double bottom = 0, double right = 0}) {
    return Padding(padding: EdgeInsets.fromLTRB(left, top, right, bottom),child: this);
  }


  Widget elevation(double radius, {double elevation = 8}) {
    return Material(
        child: this,
        elevation: elevation,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(radius)));
  }

}

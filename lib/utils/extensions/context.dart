

import 'package:ekojpubg/utils/alert/EdgeAlert.dart';
import 'package:flutter/material.dart';


extension contextUtil on BuildContext {

  showAlert(String desc) {
    EdgeAlert.show(this, title: 'Alert', description: desc, gravity: EdgeAlert.TOP, icon: Icons.warning, backgroundColor: Colors.redAccent);
  }

  showInfoAlert(String desc) {
    EdgeAlert.show(this, title: 'Info', description: desc, gravity: EdgeAlert.TOP, icon: Icons.info_outline, backgroundColor: Colors.blueGrey);
  }

}